
import argparse
import numpy as np
import pdb
import sys

from proposals import load_proposals
from proposals import get_video_size

from tubes import TYPE
from tubes import compute_mean_iou

sys.path.append('../svx/')
from datasets import DATASETS


def get_groundtruth_tube(groundtruth_loader, nr_frames):
    groundtruth = np.zeros((nr_frames, 4), dtype=TYPE)
    for frame_nr, x1, y1, x2, y2 in groundtruth_loader():
        groundtruth[frame_nr - 1] = [x1, y1, x2, y2]
    return groundtruth


def main():

    METRICS = {
        'mbao': lambda xs: 100 * max(xs),
        'corloc20': lambda xs: int(max(xs) > 0.2),
        'corloc50': lambda xs: int(max(xs) > 0.5),
    }

    parser = argparse.ArgumentParser(description="Displays proposals onto the video frames.")

    parser.add_argument('--video', help="the name of the video to work on.")
    parser.add_argument('-d', '--dataset', choices=DATASETS.keys(), required=True, help="the name of the dataset.")
    parser.add_argument('-p', '--proposals', required=True, help="path to the proposal file.")
    parser.add_argument('-m', '--metric', choices=METRICS.keys(), help="the name of the metric used for evaluation.")
    parser.add_argument('--format', default='np', choices=('np', 'mat'), help="the format of the output (NumPy `np` or Matlab `mat`).")
    parser.add_argument('-v', '--verbose', default=0, action='count', help="verbosity level.")

    args = parser.parse_args()

    dataset = DATASETS[args.dataset]
    _, _, nr_frames = get_video_size(dataset, args.video)

    proposals = load_proposals(args.proposals, args.format).reshape(-1, nr_frames, 4)
    groundtruth = get_groundtruth_tube(lambda: dataset.parse_groundtruth(args.video), nr_frames)

    tubes_iou = [compute_mean_iou(tube, groundtruth) for tube in proposals]
    print "%s %4.2f" % (args.video, METRICS[args.metric](tubes_iou))


if __name__ == '__main__':
    main()


