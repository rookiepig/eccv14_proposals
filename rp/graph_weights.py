
import argparse
import cPickle
import glob
import numpy as np
import os
import pdb
import sys

from PIL import Image

from collections import defaultdict
from matplotlib.pyplot import *
from numpy import *
from scipy import sparse
from scipy.ndimage.measurements import find_objects

from tubes import (
    area,
    get_bbox,
    union,
)

sys.path.append('../svx/')
from datasets import DATASETS
from flow_utils import readFlowFile

import colors as colutils
import disttrf as disttrf
import sparse_clustering as spclu


def get_frame_idxs(video, labels_path):

    labels_base_path = os.path.split(labels_path % 0)[0]
    _, ext = os.path.splitext(labels_path)

    frame_idxs = [
        int(os.path.splitext(f)[0])
        for f in os.listdir(labels_base_path)
        if f.endswith(ext)]

    return sorted(frame_idxs)


def load_images(generic_path, frame_idxs, data_type='image', verbose=0):

    def get_image(ii):
        return np.array(Image.open(generic_path % ii).convert('RGB'))

    res = []
    for jj, ii in enumerate(frame_idxs):

        if data_type == 'image':
            im = get_image(ii)
            im = im.astype(np.int32)
        elif data_type == 'lab': 
            im = get_image(ii)
            im = colutils.rgb_to_lab(im)
            im = rollaxis(im, 0, 3).copy()
        elif data_type == 'flow':
            if jj == 0:
                continue
            im = readFlowFile(generic_path % (ii - 1))

        res.append(im.reshape((-1, ) + im.shape))

    if verbose > 0:
        print 'Loaded %d images from %s' % (len(res), os.path.split(generic_path % ii)[0])

    return vstack(res)


def get_segmentation(labels_path, frame_idxs, verbose=0):

    all_labels = load_images(labels_path, frame_idxs, data_type='image', verbose=verbose)

    # RBG to color ID
    all_labels = (
        all_labels[:, :, :, 0] +
        all_labels[:, :, :, 1] * 256 +
        all_labels[:, :, :, 2] * 256 ** 2)

    n_labels = spclu.remap_consecutive(all_labels.ravel())

    return all_labels, n_labels
    

def get_adjacency_matrix(all_labels, n_labels):

    all_nghs = []

    # In the spatial domain.
    fake_weight = ones(all_labels[0].shape,dtype=np.float32)
    for label in all_labels:
        ngh = disttrf.ngh_labels_to_spmat(n_labels, label, fake_weight)
        ngh = ngh.tocoo()
        all_nghs.append((ngh.data,ngh.row,ngh.col))

    #
    ngh = colutils.temporal_ngh_labels_to_spmat(all_labels, n_labels)
    all_nghs.append((ngh.data, ngh.row, ngh.col))
    all_nghs.append((ngh.data, ngh.col, ngh.row))

    all_nghs = zip(*all_nghs)
    all_nghs = sparse.coo_matrix(
        (hstack(all_nghs[0]),
        (hstack(all_nghs[1]),
         hstack(all_nghs[2]))),
        shape=(n_labels, n_labels),
        dtype=np.float32)

    all_nghs = all_nghs.tocsr()
    all_nghs.sum_duplicates()

    return all_nghs


def get_distance_matrix_color(n_labels, all_labels, all_nghs, video, frame_idxs, images_path, verbose=0):

    width = all_labels.shape[-1]

    all_imgs = load_images(images_path, frame_idxs, data_type='lab', verbose=verbose)
    col_histogram = colutils.compute_color_hist(
        all_imgs.reshape(-1, width, 3),
        all_labels.reshape(-1, width),
        n_labels=n_labels,
        L_bin=20,
        ab_bin=15)

    color_comp = all_nghs.copy()
    colutils.compare_color_hist(color_comp, col_histogram, 12)

    return color_comp, all_imgs


def get_distance_matrix_flow(n_labels, all_labels, all_nghs, video, frame_idxs, flow_path, verbose=0):

    width = all_labels.shape[-1]

    all_flow = load_images(flow_path, frame_idxs, data_type='flow', verbose=verbose)
    flow_histogram = colutils.compute_flow_hist(
        all_flow.reshape(-1, width, 2),
        all_labels[1:].reshape(-1, width))

    flow_comp = all_nghs.copy()
    colutils.compare_flow_hist(flow_comp, flow_histogram, 12)

    return flow_comp, all_flow


def get_similarity_matrix_size(n_labels, all_labels, all_nghs, *args, **kwargs):

    sizes = (
        np.bincount(all_labels.ravel()) /
        float(np.prod(all_labels.shape)))

    all_nghs_coo = all_nghs.tocoo().astype(np.float32)
    all_nghs_coo.data[:] = 0

    all_nghs_coo.data = 0
    all_nghs_coo.data += sizes[all_nghs_coo.row]
    all_nghs_coo.data += sizes[all_nghs_coo.col]

    return all_nghs_coo.tocsr(), None


def get_similarity_matrix_fill(n_labels, all_labels, all_nghs, *args, **kwargs):

    def objects_to_cuboids(objects):
        return [
            get_bbox(obj[1].start, obj[0].start, obj[2].start,
                     obj[1].stop, obj[0].stop, obj[2].stop)
            for obj in objects]

    objects = find_objects(all_labels + 1)
    cuboids = objects_to_cuboids(objects)

    all_nghs_coo = all_nghs.tocoo().astype(np.float32)
    cuboid_sizes = np.zeros(all_nghs_coo.data.shape)
    video_size = float(np.prod(all_labels.shape))

    for ii, (row, col) in enumerate(zip(all_nghs_coo.row, all_nghs_coo.col)):
        cuboid = union(cuboids[row], cuboids[col])
        cuboid_size = area(cuboid) / video_size
        cuboid_sizes[ii] = cuboid_size

    sizes = np.bincount(all_labels.ravel()) / video_size

    all_nghs_coo.data = 0
    all_nghs_coo.data += sizes[all_nghs_coo.row]
    all_nghs_coo.data += sizes[all_nghs_coo.col]
    all_nghs_coo.data /= cuboid_sizes

    return all_nghs_coo.tocsr(), None


def get_similarity_matrix_size_static(n_labels, all_labels, all_nghs, *args, **kwargs):

    frame_size = np.prod(all_labels.shape[1: ])
    time_sizes = np.vstack((
        np.bincount(frame_labels.ravel(), minlength=n_labels) / float(frame_size)
        for frame_labels in all_labels)).T

    all_nghs_coo = all_nghs.tocoo().astype(np.float32)
    all_nghs_coo.data[:] = 0

    time_union = np.zeros(all_nghs_coo.data.shape)
    presence = time_sizes > 0

    for ii, (row, col) in enumerate(zip(all_nghs_coo.row, all_nghs_coo.col)):
        time_union[ii] = np.sum(presence[row] | presence[col])

    all_nghs_coo.data = 0
    all_nghs_coo.data += np.sum(time_sizes[all_nghs_coo.row], 1)
    all_nghs_coo.data += np.sum(time_sizes[all_nghs_coo.col], 1)
    all_nghs_coo.data /= time_union

    return all_nghs_coo.tocsr(), None


def get_similarity_matrix_fill_static(n_labels, all_labels, all_nghs, *args, **kwargs):

    def objects_to_bboxes(objects):
        return [[
            get_bbox(obj[1].start, obj[0].start, obj[1].stop, obj[0].stop)
            if obj is not None else None
            for obj in frame_obj]
            for frame_obj in objects]

    nr_frames = all_labels.shape[0]
    frame_size = float(np.prod(all_labels.shape[1: ]))

    time_sizes = np.vstack((
        np.bincount(frame_labels.ravel(), minlength=n_labels) / float(frame_size)
        for frame_labels in all_labels)).T
    presence = time_sizes > 0

    objects = [
        find_objects(frame_labels + 1, max_label=n_labels)
        for frame_labels in all_labels]
    bboxes = objects_to_bboxes(objects)

    all_nghs_coo = all_nghs.tocoo().astype(np.float32)
    for ii, (row, col) in enumerate(zip(all_nghs_coo.row, all_nghs_coo.col)):

        time_bbox_sizes = np.zeros(nr_frames)

        for tt in xrange(nr_frames):

            if not (presence[row][tt] or presence[col][tt]):
                bbox_size = 0
            else:
                bbox = union(bboxes[tt][row], bboxes[tt][col])
                bbox_size = area(bbox) / frame_size

            time_bbox_sizes[tt] = bbox_size

        nnz = np.nonzero(time_bbox_sizes)

        all_nghs_coo.data[ii] = np.sum(
            (time_sizes[row][nnz] + time_sizes[col][nnz]) /
            time_bbox_sizes[nnz]) / len(nnz[0])

    return all_nghs_coo.tocsr(), None


def get_similarity_matrix_size_time(n_labels, all_labels, all_nghs, *args, **kwargs):

    all_nghs_coo = all_nghs.tocoo().astype(np.float32)

    time_union = np.zeros(all_nghs_coo.data.shape)
    presence = np.vstack((
        np.bincount(frame_labels.ravel(), minlength=n_labels) > 0
        for frame_labels in all_labels)).T

    for ii, (row, col) in enumerate(zip(all_nghs_coo.row, all_nghs_coo.col)):
        time_union[ii] = np.sum(presence[row] | presence[col])

    all_nghs_coo.data = time_union / len(all_labels)

    return all_nghs_coo.tocsr(), None


def get_similarity_matrix_fill_time(n_labels, all_labels, all_nghs, *args, **kwargs):

    all_nghs_coo = all_nghs.tocoo().astype(np.float32)

    time_inter = np.zeros(all_nghs_coo.data.shape)
    time_union = np.zeros(all_nghs_coo.data.shape)
    presence = np.vstack((
        np.bincount(frame_labels.ravel(), minlength=n_labels) > 0
        for frame_labels in all_labels)).T

    for ii, (row, col) in enumerate(zip(all_nghs_coo.row, all_nghs_coo.col)):
        time_inter[ii] = np.sum(presence[row] & presence[col])
        time_union[ii] = np.sum(presence[row] | presence[col])

    all_nghs_coo.data = time_inter / time_union

    return all_nghs_coo.tocsr(), None


LOAD_DISTANCE_MATRIX = {
    'color': get_distance_matrix_color,
    'flow': get_distance_matrix_flow,
    'size': get_similarity_matrix_size,
    'fill': get_similarity_matrix_fill,
    'size_static': get_similarity_matrix_size_static,
    'fill_static': get_similarity_matrix_fill_static,
    'size_time': get_similarity_matrix_size_time,
    'fill_time': get_similarity_matrix_fill_time,
}

ALL_FEATURES = ('color', 'flow', 'size', 'fill', 'size_static', 'fill_static', 'size_time', 'fill_time')


def visualize_edge_weights(comp, all_labels, all_imgs):

    ion()

    for i,label in enumerate(all_labels):

        im = disttrf.viz_edge_weights(comp, label)
        subplot(211)

        imshow(colutils.lab_to_rgb(rollaxis(all_imgs[i],2,0).copy()))
        subplot(212)

        imshow(im,cmap=cm.gray)
        pdb.set_trace()


def save_graph(all_labels, adjacency_matrix, outpath):

    data = adjacency_matrix.data
    indices = adjacency_matrix.indices
    indptr = adjacency_matrix.indptr
    _, positions = np.unique(all_labels, return_index=True)

    
    with open(outpath, 'w') as ff:

        for ii in range(len(indptr) - 1):
            for jj in range(indptr[ii], indptr[ii + 1]):

                row = ii
                col = indices[jj]

                if row > col:
                    continue

                assert row == all_labels.ravel()[positions[row]]
                assert col == all_labels.ravel()[positions[col]]
                print >> ff, positions[row], positions[col], data[jj]


def load_edge_weights():

    outpath = "../data/weights.pickle"

    with open(outpath, 'r') as ff:
        weights_dict, bias = cPickle.load(ff)

    return weights_dict, bias


def main():

    parser = argparse.ArgumentParser(description="Computes graph weights based on color, flow and geometric features.")

    parser.add_argument('--video', help="the name of the video to work on.")
    parser.add_argument('-d', '--dataset', choices=DATASETS.keys(), required=True, help="the name of the dataset.")
    parser.add_argument('-f', '--features', choices=ALL_FEATURES, nargs='+', default=ALL_FEATURES, help="type of features to use for learning the edge weights.")
    parser.add_argument('-l', '--level', type=int, help="the level of the segmentation.")
    parser.add_argument('--show', default=False, action='store_true', help="show plots.")
    parser.add_argument('--write', default=False, action='store_true', help="overwrite graph weights.")
    parser.add_argument('-v', '--verbose', action='count', default=0, help="verbosity level.")

    args = parser.parse_args()

    dataset = DATASETS[args.dataset]
    outpath = dataset.get_graph_path(args.video, args.level, args.features)

    if os.path.exists(outpath) and not args.write:
        print "Path exists", outpath
        sys.exit(1)

    if args.verbose > 0:
        print "Working on video %s at level %2d." % (args.video, args.level)

    labels_path = dataset.get_generic_segmentation_path(args.video, args.level)
    frame_idxs = get_frame_idxs(args.video, labels_path)
    all_labels, n_labels = get_segmentation(labels_path, frame_idxs, verbose=(args.verbose - 1))
    all_nghs = get_adjacency_matrix(all_labels, n_labels)

    LOADER_ARGS = defaultdict(tuple)
    LOADER_ARGS['color'] = (frame_idxs, dataset.get_generic_images_path(args.video))
    LOADER_ARGS['flow'] = (frame_idxs, dataset.get_generic_flow_path(args.video))

    csr_data_dict = {}
    for feature in args.features:
        csr_data_dict[feature] = LOAD_DISTANCE_MATRIX[feature](
            n_labels,
            all_labels,
            all_nghs,
            args.video,
            *LOADER_ARGS[feature],
            verbose=(args.verbose - 1))[0]

    csr_data = sparse.csr_matrix((
        np.zeros(all_nghs.data.shape),
        all_nghs.indices,
        all_nghs.indptr))

    def sigmoid(X):
      return 1 / (1 + np.exp(-X))

    weights_dict, bias = load_edge_weights()
    data = None
    for feature in args.features:
        weighted_data = weights_dict[feature] * csr_data_dict[feature].data
        if data is None:
            data = weighted_data
        else:
            data += weighted_data
    csr_data.data = 1 - sigmoid(data + bias)

    save_graph(all_labels, csr_data, outpath)

    if args.show:
        visualize_edge_weights(csr_data, all_labels, all_imgs)


if __name__ == '__main__':
    main()

