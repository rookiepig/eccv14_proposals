
import argparse
import itertools
import networkx as nx
import numpy as np
import os
import random
import pdb
import scipy.io
import subprocess
import sys

from PIL import Image
from collections import defaultdict
from scipy.ndimage.measurements import find_objects

from tubes import (
    TYPE,
    get_bbox,
    intersection,
    union,
)

sys.path.append('../svx/')
from datasets import DATASETS


random.seed(0)
np.random.seed(0)


ALL_FEATURES = ('color', 'flow', 'size', 'fill', 'size_static', 'fill_static', 'size_time', 'fill_time')


SOURCE = -1
TARGET = -2


## Utilities.
def invert_map(map_dict):
    return {vv: kk for kk, vv in map_dict.iteritems()}


def get_image_size(image_path):
    return Image.open(image_path).size


## Random prim algorithm.
def random_path(A, D_source, D_target, seed_idx, no_temporal_constraint, verbose=0):

    def softmax(ww):
        """Softmax over all elements."""
        pp = np.exp(ww - np.max(ww))
        pp /= np.sum(pp)
        return pp

    def print_frequency(verbose):
        if verbose == 3:
            return 500
        elif verbose == 4:
            return 100
        elif verbose == 5:
            return 50
        else:
            return 1

    def stop(proba):
        return proba > np.random.rand()

    def temporal_constraint(best_dist_to_source, best_dist_to_target):
        return (
            no_temporal_constraint or
            best_dist_to_source + best_dist_to_target == 0)

    try:
        # Prefer to use Yael, as indexing is very slow in NumPy.
        from yael import ynumpy
        def extract_rows_cols(A, rs, cs):
            return ynumpy.extract_rows_cols(A, rs, cs)
    except ImportError:
        def extract_rows_cols(A, rs, cs):
            return A[np.ix_(rs, cs)]

    iter = 0
    MAX = 1

    S = np.zeros(len(A), dtype=np.bool_)
    S[seed_idx] = True

    best_dist_to_source = D_source[seed_idx]
    best_dist_to_target = D_target[seed_idx]

    while True:

        inside = np.where(S)[0].astype(np.int32)
        outside = np.where(~S)[0].astype(np.int32)

        # Find neighbours connected to current selection.
        in_out = extract_rows_cols(A, inside, outside)
        neighbors = outside[np.min(in_out, axis=0) < MAX]

        energy = 1 - extract_rows_cols(A, neighbors, inside)
        energy = energy.astype(np.float64)  # In order to get the probabilities to sum to one.
        proba = energy.ravel() / energy.sum()

        out_idx, in_idx = np.unravel_index(np.random.choice(len(proba), p=proba), energy.shape)

        if temporal_constraint(best_dist_to_source, best_dist_to_target) and stop(energy[out_idx, in_idx]):
            break

        S[neighbors[out_idx]] = True

        best_dist_to_source = np.min(D_source[S])
        best_dist_to_target = np.min(D_target[S])

        if verbose > 2 and iter % print_frequency(verbose) == 0:
            print "Iteration %6d" % iter
            print "\tBest distance to source:", best_dist_to_source
            print "\tBest distance to target:", best_dist_to_target

        iter += 1

    if verbose > 1:
        print "The random walk selected %d nodes." % np.sum(S)
        print "The random walk lasted for %d iterations." % iter

    return S


def get_proposals(graph, segmentation, node_to_color_map, nr_proposals, no_temporal_constraint, verbose=0):

    def get_frame_to_node_and_bbox(segmentation, color_to_node):

        frame_to_node_and_bbox = defaultdict(list)

        for ii in xrange(segmentation.shape[2]):

            frame_segmentation = segmentation[:, :, ii]

            for color in np.unique(frame_segmentation):

                if color not in color_to_node:
                    continue

                obj = find_objects(frame_segmentation == color)[0]

                node = color_to_node[color]
                bbox = get_bbox(obj[1].start, obj[0].start, obj[1].stop, obj[0].stop)

                frame_to_node_and_bbox[ii].append((node, bbox))

        return frame_to_node_and_bbox

    def generate_paths():

        def get_random_node(graph):
            while True:
                yield random.choice(graph.nodes())

        def graph_remove(graph, node):
            graph2 = graph.copy()
            graph2.remove_node(node)
            return graph2

        dist_to_source, _ = nx.single_source_dijkstra(graph_remove(graph, TARGET), SOURCE, weight='weight')
        dist_to_target, _ = nx.single_source_dijkstra(graph_remove(graph, SOURCE), TARGET, weight='weight')

        graph.remove_node(SOURCE)
        graph.remove_node(TARGET)

        D_source = np.array([dist_to_source[node] for node in graph.nodes()])
        D_target = np.array([dist_to_target[node] for node in graph.nodes()])

        node_to_idx = {node: idx for idx, node in enumerate(graph.nodes())}
        idx_to_node = {idx: node for idx, node in enumerate(graph.nodes())}

        A = np.array(nx.to_numpy_matrix(graph).astype(np.float32))
        # Hack: A distance of 1 is equivalent to disconnected components. If we
        # allow disconnected components, then we cannot enforce the temporal
        # constraint.
        A[A == 1] = 0.999
        A[A == 0] = 1

        def get_path(A, seed_idx):
            S = random_path(
                A,
                D_source,
                D_target,
                seed_idx,
                no_temporal_constraint=no_temporal_constraint,
                verbose=(verbose - 1))
            return [idx_to_node[idx] for idx in np.where(S)[0]]

        if verbose > 2:
            print "--> Running random path two-way."
            print "\tTotal number of nodes in graph", len(graph.nodes())

        for seed_node in get_random_node(graph):
            if seed_node in (SOURCE, TARGET):
                continue
            if verbose > 2:
                print "\tSeed node", seed_node
            seed_idx = node_to_idx[seed_node]
            yield get_path(A, seed_idx)

    if verbose > 1:
        print "\tNumber of super-voxels in graph", len(graph.nodes())

    frame_to_node_and_bbox = get_frame_to_node_and_bbox(segmentation, invert_map(node_to_color_map))

    nr_frames = segmentation.shape[2]
    proposals = np.zeros((nr_proposals, nr_frames, 4), dtype=TYPE)

    colors = []

    for iter, nodes_path in enumerate(generate_paths()):

        if nr_proposals is not None and iter >= nr_proposals:
            break

        if verbose > 3:
            print "\t\tIteration: %5d" % iter

        for idx in xrange(nr_frames):

            selected_bboxes = [
                bbox for node, bbox in frame_to_node_and_bbox[idx]
                if node in nodes_path]
            frame_proposal = reduce(union, selected_bboxes, None)

            if frame_proposal is not None:
                proposals[iter, idx] = frame_proposal

        colors.append([
            node_to_color_map[node]
            for node in nodes_path
            if node not in (SOURCE, TARGET)])

    return proposals, colors


def get_video_size(dataset, video):
    images_dir = dataset.get_images_path(video)
    images_paths = [os.path.join(images_dir, f) for f in os.listdir(images_dir)]
    width, height = get_image_size(images_paths[0])
    nr_frames = len(images_paths)
    return width, height, nr_frames


def load_graph(infile):

    def parse_line(line):
        node_1, node_2, weight = line.split()
        node_1 = int(node_1)
        node_2 = int(node_2)
        weight = float(weight)
        return node_1, node_2, weight

    graph = nx.Graph()
    with open(infile, 'r') as ff:
        for line in ff.readlines():
            node_1, node_2, weight = parse_line(line)
            edge_data = graph.get_edge_data(node_1, node_2)
            if edge_data is not None and weight > edge_data['weight']:
                continue
            graph.add_edge(node_1, node_2, weight=weight)

    return graph


def load_segmentation(paths, video_size, verbose=0):

    def get_id(red, green, blue):
        return 256 ** 2 * red + 256 * green + blue

    width, height, nr_frames = video_size
    segmentation = np.zeros((height, width, nr_frames), dtype=np.uint32)

    assert nr_frames == len(paths), "Number of video frames (%d) and segmentation frames (%d) do not match" % (nr_frames, len(paths))

    for tt, path in enumerate(paths):

        if verbose > 2:
            print "%3d %s" % (tt, path)

        frame = np.array(Image.open(path).convert('RGB'))
        segmentation[:, :, tt] = get_id(frame[:, :, 0],
                                        frame[:, :, 1],
                                        frame[:, :, 2])

    return segmentation


def load_data(dataset, video, level, features, verbose):

    def print_node_to_color_map(node_to_color_map):
        print "%10s %10s" % ("Node ID", "Color ID")
        for node, color in node_to_color_map.iteritems():
            print "%10d %10d" % (node, color)

    def node_to_pos(node_id, video_size):
        yy, xx, tt = np.unravel_index(node_id, video_size, order='F')
        return xx, yy, tt

    def node_to_color(node_id, segmentation, video_size):
        return segmentation[node_to_pos(node_id, video_size)]

    def get_nodes_in_frame(frame_nr, segmentation, node_to_color_map):
        return [
            node
            for node, color in node_to_color_map.iteritems()
            if color in segmentation[:, :, frame_nr]]

    def add_source_and_sink(graph, nodes_in_first_frame, nodes_in_last_frame, video_size, verbose=0):
        for node in graph.nodes():
            if verbose > 2:
                print "\tNode ID %10d -->" % node
            if node in nodes_in_first_frame:
                graph.add_edge(node, SOURCE, weight=0.)
            if node in nodes_in_last_frame:
                graph.add_edge(node, TARGET, weight=0.)

    video_size = get_video_size(dataset, video)
    frame_paths = dataset.get_segmentation_paths(video, level)

    graph = load_graph(dataset.get_graph_path(video, level, features))
    segmentation = load_segmentation(frame_paths, video_size, verbose=verbose)

    node_to_color_map = {
        node: node_to_color(node, segmentation, video_size)
        for node in graph.nodes()}

    if verbose > 3:
        print_node_to_color_map(node_to_color_map)

    nodes_in_first_frame = get_nodes_in_frame(0, segmentation, node_to_color_map)
    nodes_in_last_frame = get_nodes_in_frame(-1, segmentation, node_to_color_map)
    add_source_and_sink(graph, nodes_in_first_frame, nodes_in_last_frame, video_size, verbose=verbose)

    return graph, segmentation, node_to_color_map


def get_proposals_path(proposals_dir, level, features, temporal_constraint, format):
    EXT = {
        'np': 'dat',
        'mat': 'mat',
    }
    return os.path.join(
        proposals_dir,
        'proposals_level_%d_features_%s_no_temp_constraint_%s.%s' % (
            level,
            '_'.join(features),
            temporal_constraint,
            EXT[format]))


def save_proposals(path, proposals, format, verbose=0):
    if format == 'np':
        proposals.tofile(path)
    elif format == 'mat':
        scipy.io.savemat(path, dict(proposals=proposals))
    else:
        assert False, "Unknown format %s." % format
    if verbose:
        print 'Proposals saved @', path


def load_proposals(path, format):
    if format == 'np':
        proposals = np.fromfile(path, dtype=TYPE)
    elif format == 'mat':
        proposals = scipy.io.loadmat(path)['proposals']
    else:
        assert False, "Unknown format %s." % format
    return proposals


def main():

    parser = argparse.ArgumentParser(description="Generate spatio-temporal object proposals.")

    parser.add_argument('--video', help="the name of the video to work on.")
    parser.add_argument('-d', '--dataset', choices=DATASETS.keys(), required=True, help="the name of the dataset.")
    parser.add_argument('-f', '--features', choices=ALL_FEATURES, nargs='+', default=ALL_FEATURES, help="type of features to use for learning the edge weights.")
    parser.add_argument('-l', '--level', type=int, help="the level of the segmentation.")
    parser.add_argument('-n', '--nr_proposals', type=int, help="the number of random proposals to be generated.")
    parser.add_argument('--no_temporal_constraint', default=False, action='store_true', help="do not force the tube to cover all video.")
    parser.add_argument('--format', default='np', choices=('np', 'mat'), help="the format of the output (NumPy `np` or Matlab `mat`).")
    parser.add_argument('-v', '--verbose', default=0, action='count', help="verbosity level.")

    args = parser.parse_args()

    dataset = DATASETS[args.dataset]
    graph, segmentation, node_to_color_map = load_data(dataset, args.video, args.level, args.features, verbose=args.verbose)


    proposals, _ = get_proposals(graph, segmentation, node_to_color_map, args.nr_proposals, args.no_temporal_constraint, args.verbose)

    proposals_dir = dataset.get_proposal_directory(args.video)
    if not os.path.exists(proposals_dir):
        os.makedirs(proposals_dir)

    path = get_proposals_path(proposals_dir, args.level, args.features, args.no_temporal_constraint, args.format)
    save_proposals(path, proposals, args.format, verbose=args.verbose)


if __name__ == '__main__':
    main()

