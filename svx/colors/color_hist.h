#include "numpy_image.h"


/* Compute a color histogram based for each superpixel
*/
void _compute_color_hist( float_cube* lab, int_image* labels, const int nl, int L_bin, int ab_bin, csr_matrix* res_out );

/* Compare color histograms of neighboring superpixels.
  dist_type = 1:'l1', 2:'l2', 0:'intersection', 12:'chi2'
*/
void compare_color_hist( csr_matrix* ngh, const csr_matrix* hist, int dist_type );


/* Compute a flow histogram based for each superpixel
  flow_pow = 0.2 ?
  ninth = additional constant dimension, defined per-pixel
*/
void _compute_flow_hist( float_cube* flow, int_image* labels, const int nl, 
                         float flow_pow, float ninth, float_image* res );


/* Compare color histograms of neighboring superpixels.
  dist_type = 1:'l1', 2:'l2', 0:'intersection', 12:'chi2'
*/
void compare_flow_hist( csr_matrix* ngh, const float_image* hist, const int dist_type );



/* connect temporal neighbours
*/
void _temporal_ngh_labels_to_spmat( const int n_labels, const int_layers* labels, coo_matrix* res_out );
