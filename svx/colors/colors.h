#include "numpy_image.h"


/* Convert an rgb image into the Lab* color space
*/
void _rgb_to_lab_layers( UBYTE_image3* img, float_layers* res, float color_attenuation );
void _rgb_to_lab_cube( UBYTE_image3* img, float_cube* res, float color_attenuation );


/* Backward conversion of from the Lab* color space to RGB
*/
void _lab_layers_to_rgb( float_layers* img, UBYTE_image3* res );
void _lab_cube_to_rgb( float_cube* img, UBYTE_image3* res );

