
import os
import pdb

from PIL import Image

import numpy as np
import sparse_clustering as spclu
from scipy import sparse


def sqgrad(img, dim=0):
    if dim == -1:
        return np.sum([np.sum(np.array(np.gradient(img[:, :, f]))**2, 0) for f in range(img.shape[dim])], axis=0)
    elif dim == 0:
        return np.sum([np.sum(np.array(np.gradient(img[f, :,:]))**2, 0) for f in range(img.shape[dim])], axis=0)
    assert False


def load_flow(flow_paths, name, shape=None, verbose=0):
    import flow_utils
    try:
        path = os.path.join(flow_paths['forward'], name)
        flow = flow_utils.readFlowFile(path)
        if verbose:
            print 'loading flow from', path
    except IOError:
        flow = None
    try:
        path = os.path.join(flow_paths['backward'], name)
        rev = flow_utils.readFlowFile(path)
        if verbose:
            print 'loading flow from', path
    except IOError:
        rev = None
    if flow != None:
        if rev != None:
            return (flow - rev) / 2
        else:
            return flow
    else:
        if rev == None:
            if not shape:
                assert False, "error: no flow found at " + path
            else:
                rev = zeros(shape + (2,), dtype=np.float32)
        return rev


def zeros32(n):
    return np.zeros(n, dtype=np.int32)


def ones32(n):
    return np.ones(n, dtype=np.int32)


def enlarge(csr, offset, size):
    nr, nc = csr.shape
    assert nr == nc and nr + offset <= size
    indptr = np.hstack((zeros32(offset), csr.indptr, csr.indptr[-1] * ones32(size - nr - offset)))
    indices = csr.indices + offset
    return sparse.csr_matrix((csr.data, indices, indptr), shape=(size, size), dtype=np.float32)


def sparse_vstack(mats):
    indptr = []
    indices = []
    data = []
    nr = 0
    nc = mats[0].shape[1]
    lastptr = 0
    for m in mats:
        nr += m.shape[0]
        assert nc == m.shape[1]
        indptr.append(lastptr + m.indptr[:-1])
        lastptr += m.indptr[-1]
        indices.append(m.indices)
        data.append(m.data)
    indptr.append([lastptr])
    return sparse.csr_matrix((np.hstack(data), np.hstack(indices), np.hstack(indptr)), shape=(nr, nc), dtype='float32')


def check_symmetric(spmat):
    diff = spmat - spmat.T
    assert diff.nnz == 0, not exit and pdb.set_trace()


def remove_small_clusters(n_clusters, n_node, tree, th=2, start=0):
    nc, final_labels = spclu.tree_assign(n_clusters, n_node, tree)
    if start >= len(tree) / 2:
        return nc, final_labels  # security
    clusters_size = np.bincount(final_labels, minlength=n_clusters)

    # recompute the same thing before remapping
    nb, true_labels = spclu.tree_assign(n_clusters, n_node, tree, remap=0)

    place_first = np.zeros(len(tree), dtype=np.bool)
    for n in (clusters_size <= th).nonzero()[0]:
        n = np.unique(true_labels[final_labels == n])
        assert len(n) == 1
        n = n[0]
        # find when it's connected in the graph
        u = (tree[start:, 0] == n).argmax()
        v = (tree[start:, 1] == n).argmax()
        #assert u or v,  pdb.set_trace()
        if u == v == 0:
            continue
        if u == 0:
            u = len(tree)
        if v == 0:
            v = len(tree)
        place_first[start + min(u, v)] = True

    if any(place_first):
        tree = np.vstack((tree[place_first], tree[~place_first]))
        return remove_small_clusters(n_clusters, n_node, tree, th=th, start=start + sum(place_first))
    else:
        return nc, final_labels


def clean_tree_assign(n_min_node, n_clusters, n_node, tree):
    # return the actual n_clusters that should be used in tree_assign
    # to obtain n_clusters without counting noisy ones.
    nsup = 0
    while n_clusters + nsup <= len(tree):
        nc, final_labels = spclu.tree_assign(n_clusters + nsup, n_node, tree)
        nb = np.bincount(final_labels, minlength=n_clusters)
        nbad = sum(n_min_node >= nb)  # count 'fake' clusters
        if nc - nbad >= n_clusters:
            return nc
        nsup = int(nbad)
    return len(tree) + 1


def save_result_nc_ppm(all_data, n_clusters, savedir, clean=0, verbose=0):

    images_names = all_data['images_names']
    graph = all_data['graph']
    tree = all_data['tree']
    time_penalty = all_data['time_penalty']
    time_penalty_mode = all_data['time_penalty_mode']
    node_frames = all_data['node_frames']
    all_nodes = all_data['all_nodes']
    labels = all_data['labels']
    n_node = len(node_frames)

    if not os.path.exists(savedir):
        os.makedirs(savedir)

    if clean >= 0:
        n_clusters = clean_tree_assign(clean, n_clusters, n_node, tree)

    if clean >= 0:
        tree, edges = spclu.temporal_tree_cluster(graph, node_frames, penalty_coef=time_penalty, mode=time_penalty_mode, linkage='a', output_edges_at=n_clusters)
        n_clusters, final_labels = spclu.tree_assign(0, n_node, tree)

        # Save weight matrix.
        def node_to_xyt(node):
            x, y = all_nodes[node, :2].astype(np.int32)
            t = node_frames[node]
            assert labels[t, y, x] == node
            return np.ravel_multi_index((x, y, t), labels.shape[::-1], order='F')

        f = open(os.path.join(savedir, 'graph_weights.txt'), 'w')
        for i, j, w in zip(edges.row, edges.col, edges.data):
            print >>f, '%d %d %f' % (node_to_xyt(i), node_to_xyt(j), w)

    else:
        n_clusters, final_labels = remove_small_clusters(n_clusters, n_node, tree, th=-clean)

    random_rgb = np.random.randint(256, size=(n_clusters, 3)).astype(np.uint8)
    for i, name in enumerate(images_names):
        label = final_labels[labels[i]]
        label = random_rgb[label]
        Image.fromarray(label).save(os.path.join(savedir, name[:-3] + 'png'))

    if verbose:
        print 'Result for %5d clusters saved in %s' % (n_clusters, savedir)


def save_result_ppm(segmentation_dir, all_data, clean_clusters=0, list_nc=None, verbose=0):
    if not list_nc:
        if clean_clusters:
            list_nc = [2, 3, 4, 5, 8, 10, 12, 14, 16, 20, 25, 30, 40, 50, 60, 70, 80, 100, 150, 200, 300, 400, 500]
        else:
            list_nc = [2, 3, 4, 5, 8, 10, 12, 14, 16, 20, 25, 30, 40, 50, 60, 70, 80, 100, 150, 200, 300, 400, 500, 600, 750, 1000, 1250, 1500, 1750, 2000, 2500, 3000]
    for n_clusters in list_nc:
        path = os.path.join(segmentation_dir, '%d' % n_clusters)
        save_result_nc_ppm(all_data, n_clusters, path, clean=clean_clusters, verbose=verbose)


def save_hierarchy(save_path, tree, labels, verbose=0):
    np.savez_compressed(save_path, tree=tree, labels=labels)
    if verbose:
        print 'Saved hierarchy at', save_path
