#ifndef ___DISJOINT_SET___
#define ___DISJOINT_SET___

// find the representative of the set x belongs to
inline int FindRepresentative( int* parents, int x) {
  if( parents[x] != x )
    parents[x] = FindRepresentative(parents,parents[x]);
  return parents[x];
}

// check whether two elements are in the same set
inline int SameSet( int* parents, int x, int y ) {
  int xRoot = FindRepresentative(parents,x);
  int yRoot = FindRepresentative(parents,y);
  return (xRoot == yRoot);
}

// union to sets
// return 0 if x and y were already in the same set
inline int Union( int* parents, int* ranks, int x, int y) {
  int xRoot = FindRepresentative(parents,x);
  int yRoot = FindRepresentative(parents,y);
  if(xRoot == yRoot)  return 0; // already in the same set
  
  if( ranks[xRoot] < ranks[yRoot] )
     parents[xRoot] = yRoot;
  else if( ranks[xRoot] > ranks[yRoot] )
     parents[yRoot] = xRoot;
  else {
     parents[yRoot] = xRoot;
     ranks[xRoot]++;
   }
   return 1;
}


#endif
