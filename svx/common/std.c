#include "std.h"

#ifdef __cplusplus
#else
inline float pow2( float f )  {return f*f;}
#include <stdint.h>
inline unsigned int fastlog2(const unsigned int x) {
  unsigned int y;
  asm ( "\tbsr %1, %0\n"
      : "=r"(y)
      : "r" (x)
  );
  return y;
}
inline unsigned int upper2(unsigned int x)  {
  if(x<=1)  return 1;
  return 1<<(1+fastlog2(x-1));
}
inline unsigned int upper23(unsigned int x) {
  // return the closest upper number which is of the form 2^x * 3^y
  int r1 = upper2(x) - x;
  int r3 = 3*upper2(1+(x-1)/3) - x;
  int r9 = 9*upper2(1+(x-1)/9) - x;
  int r27=27*upper2(1+(x-1)/27) - x;
  int best = r1;
  if(r3<best) best = r3;
  if(r9<best) best = r9;
  if(r27<best) best = r27;
  best += x;
  assert((best%2)==0); // must be multiple of 2
  return best;
}
#endif

/* Some sample C code for the quickselect algorithm, 
   taken from Numerical Recipes in C. */

#undef SWAP
#define SWAP(a,b) {temp=(a);(a)=(b);(b)=temp;}

int fastmedian_i(int *arr, int n, int k) {
  long i,ir,j,l,mid;
  int a,temp=0;

  l=0;
  ir=n-1;
  for(;;) {
    if (ir <= l+1) { 
      if (ir == l+1 && arr[ir] < arr[l])  SWAP(arr[l],arr[ir]);
      return arr[k];
    }
    else {
      mid=(l+ir) >> 1; 
      SWAP(arr[mid],arr[l+1]);
      if (arr[l] > arr[ir])   SWAP(arr[l],arr[ir]);
      if (arr[l+1] > arr[ir]) SWAP(arr[l+1],arr[ir]);
      if (arr[l] > arr[l+1])  SWAP(arr[l],arr[l+1]);
      i=l+1; 
      j=ir;
      a=arr[l+1]; 
      for (;;) { 
        do i++; while (arr[i] < a); 
        do j--; while (arr[j] > a); 
        if (j < i) break; 
        SWAP(arr[i],arr[j]);
      } 
      arr[l+1]=arr[j]; 
      arr[j]=a;
      if (j >= k) ir=j-1; 
      if (j <= k) l=i;
    }
  }
}

float fastmedian_f(float *arr, int n, int k) {
  long i,ir,j,l,mid;
  float a,temp=0;

  l=0;
  ir=n-1;
  for(;;) {
    if (ir <= l+1) { 
      if (ir == l+1 && arr[ir] < arr[l])  SWAP(arr[l],arr[ir]);
      return arr[k];
    }
    else {
      mid=(l+ir) >> 1; 
      SWAP(arr[mid],arr[l+1]);
      if (arr[l] > arr[ir])   SWAP(arr[l],arr[ir]);
      if (arr[l+1] > arr[ir]) SWAP(arr[l+1],arr[ir]);
      if (arr[l] > arr[l+1])  SWAP(arr[l],arr[l+1]);
      // now arr[l] <= arr[l+1] <= arr[ir]
      i=l+1; 
      j=ir;
      a=arr[l+1]; 
      for (;;) { 
        do i++; while (arr[i] < a); 
        do j--; while (arr[j] > a); 
        if (j < i) break; 
        SWAP(arr[i],arr[j]);
      } 
      arr[l+1]=arr[j]; 
      arr[j]=a;
      if (j >= k) ir=j-1; 
      if (j <= k) l=i;
    }
  }
}


/*
int isfinite(double d) {
  if(d!=d)  return 0;
  if(d==d+1)  return 0;
  return 1;
}*/

void check_finite(const char* name, const float* vec, int nb, int do_assert) {
  int n;
  for(n=0; n<nb; n++) {
    float v = vec[n];
    if( v!=v ) {
      printf("warning: NaN value in %s at position %d\n",name,n);
      if(do_assert)  assert(0);
    }
    if( v+1==v ) {
      printf("warning: inf value in %s at position %d\n",name,n);
      if(do_assert)  assert(0);
    }
  }
}

















