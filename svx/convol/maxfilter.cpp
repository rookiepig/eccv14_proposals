#include "std.h"
#include "maxfilter.h"
#include "omp.h"


 /*****************************************************\
 |                  Maxima detection                   |
 \*****************************************************/

/* Horizontal max-filtering (in-place is ok)
*/
template<typename type_t>
void _max_filter_3_horiz( int tx, int ty, type_t* img, type_t* res ) {
  int j;
  
  #if defined(USE_OPENMP)
  #pragma omp parallel for
  #endif
  for(j=0; j<ty; j++) {
    int i;
    type_t *p = img + j*tx;
    type_t *r = res + j*tx;
    
    type_t m = MAX(p[0],p[1]);
    *r++ = m;
    
    for(i=1; i<tx-1; i++) {
      type_t m2 = MAX(p[i],p[i+1]);
      *r++ = MAX(m,m2);
      m=m2;
    }
    
    *r++ = m;
  }
}

/* Vertical max-filtering (in-place is ok)
*/
template<typename type_t>
void _max_filter_3_vert( int tx, int ty, type_t* img, type_t* res ) {
  int n;
  #define N 8
  
  #if defined(USE_OPENMP)
  #pragma omp parallel for
  #endif
  for(n=0; n<N; n++) {
    int j;
    for(j=0; j<ty-1; j++) {
      int i, imin=n*tx/N, imax=(n+1)*tx/N;
      type_t *p = img + j*tx;
      type_t *r = res + j*tx + imin;
      
      for(i=imin; i<imax; i++) {
        *r++ = MAX(p[i],p[i+tx]);
      }
    }
  }
  memcpy(res+(ty-1)*tx,res+(ty-2)*tx,tx*sizeof(type_t)); // copy last row
  
  #if defined(USE_OPENMP)
  #pragma omp parallel for
  #endif
  for(n=0; n<N; n++) {
    int j;
    for(j=ty-2; j>0; j--) {
      int i, imin=n*tx/N, imax=(n+1)*tx/N;
      type_t *p = res + (j-1)*tx;
      type_t *r = res + j*tx + imin;
      
      for(i=imin; i<imax; i++) {
        type_t r0 = *r;
        *r++ = MAX(r0,p[i]);
      }
    }
  }
}

template<typename type_t>
void _max_filter_3( int tx, int ty, type_t* img, char ori, type_t* res ) {
  if(ori!='h') {
    _max_filter_3_vert(tx,ty,img,res);
    if(ori!='v')  _max_filter_3_horiz(tx,ty,res,res);
  } else
    _max_filter_3_horiz(tx,ty,img,res);
}

void _max_filter_3_f( float_image* img, char ori, float_image* res ) {
  ASSERT_SAME_SIZE(img,res);
  _max_filter_3( img->tx, img->ty, img->pixels, ori, res->pixels);
}
void _max_filter_3_i( int_image* img, char ori, int_image* res ) {
  ASSERT_SAME_SIZE(img,res);
  _max_filter_3( img->tx, img->ty, img->pixels, ori, res->pixels);
}
void _max_filter_3_B( UBYTE_image* img, char ori, UBYTE_image* res ) {
  ASSERT_SAME_SIZE(img,res);
  _max_filter_3( img->tx, img->ty, img->pixels, ori, res->pixels);
}

void _max_filter_3_layers_f( float_layers* img, char ori, float_layers* res ) {
  ASSERT_SAME_LAYERS_SIZE(img,res);
  const int tx = img->tx;
  const int ty = img->ty;
  
  int l;
  #if defined(USE_OPENMP)
  omp_set_nested(0);  // no parallellization inside the loop
  omp_set_dynamic(0);
  #pragma omp parallel for
  #endif
  for(l=0; l<img->tz; l++)
    _max_filter_3( tx,ty, img->pixels+l*tx*ty, ori, res->pixels+l*tx*ty);
}


/* Horizontal min-filtering (in-place is ok)
*/
template<typename type_t>
void _min_filter_3_horiz( int tx, int ty, type_t* img, type_t* res ) {
  int j;
  
  #if defined(USE_OPENMP)
  #pragma omp parallel for
  #endif
  for(j=0; j<ty; j++) {
    int i;
    type_t *p = img + j*tx;
    type_t *r = res + j*tx;
    
    type_t m = MIN(p[0],p[1]);
    *r++ = m;
    
    for(i=1; i<tx-1; i++) {
      type_t m2 = MIN(p[i],p[i+1]);
      *r++ = MIN(m,m2);
      m=m2;
    }
    
    *r++ = m;
  }
}

/* Vertical min-filtering (in-place is ok)
*/
template<typename type_t>
void _min_filter_3_vert( int tx, int ty, type_t* img, type_t* res ) {
  int n;
  #define N 8
  
  #if defined(USE_OPENMP)
  #pragma omp parallel for
  #endif
  for(n=0; n<N; n++) {
    int j;
    for(j=0; j<ty-1; j++) {
      int i, imin=n*tx/N, imax=(n+1)*tx/N;
      type_t *p = img + j*tx;
      type_t *r = res + j*tx + imin;
      
      for(i=imin; i<imax; i++) {
        *r++ = MIN(p[i],p[i+tx]);
      }
    }
  }
  memcpy(res+(ty-1)*tx,res+(ty-2)*tx,tx*sizeof(type_t)); // copy last row
  
  #if defined(USE_OPENMP)
  #pragma omp parallel for
  #endif
  for(n=0; n<N; n++) {
    int j;
    for(j=ty-2; j>0; j--) {
      int i, imin=n*tx/N, imax=(n+1)*tx/N;
      type_t *p = res + (j-1)*tx;
      type_t *r = res + j*tx + imin;
      
      for(i=imin; i<imax; i++) {
        type_t r0 = *r;
        *r++ = MIN(r0,p[i]);
      }
    }
  }
}

template<typename type_t>
void _min_filter_3( int tx, int ty, type_t* img, char ori, type_t* res ) {
  if(ori!='h') {
    _min_filter_3_vert(tx,ty,img,res);
    if(ori!='v')  _min_filter_3_horiz(tx,ty,res,res);
  } else
    _min_filter_3_horiz(tx,ty,img,res);
}

void _min_filter_3_f( float_image* img, char ori, float_image* res ) {
  ASSERT_SAME_SIZE(img,res);
  _min_filter_3( img->tx, img->ty, img->pixels, ori, res->pixels);
}
void _min_filter_3_i( int_image* img, char ori, int_image* res ) {
  ASSERT_SAME_SIZE(img,res);
  _min_filter_3( img->tx, img->ty, img->pixels, ori, res->pixels);
}
void _min_filter_3_B( UBYTE_image* img, char ori, UBYTE_image* res ) {
  ASSERT_SAME_SIZE(img,res);
  _min_filter_3( img->tx, img->ty, img->pixels, ori, res->pixels);
}

void _min_filter_3_layers_f( float_layers* img, char ori, float_layers* res ) {
  ASSERT_SAME_LAYERS_SIZE(img,res);
  const int tx = img->tx;
  const int ty = img->ty;
  
  int l;
  #if defined(USE_OPENMP)
  omp_set_nested(0);  // no parallellization inside the loop
  omp_set_dynamic(0);
  #pragma omp parallel for
  #endif
  for(l=0; l<img->tz; l++)
    _min_filter_3( tx,ty, img->pixels+l*tx*ty, ori, res->pixels+l*tx*ty);
}


 /*****************************************************\
 |                     Subsampling                     |
 \*****************************************************/


/* Subsample an array, equivalent to res = img[:,::2,::2]
*/
void _subsample2_f( float_image* img, float_image* res ) {
  const int tx = res->tx;
  const int ty = res->ty;
  assert( (img->tx+1)/2 == tx );
  assert( (img->ty+1)/2 == ty );
  
  int x,y;
  for(y=0; y<ty; y++) {
    float* i = img->pixels + (2*y)*img->tx ;
    float* r = res->pixels +    y *tx;
    for(x=0; x<tx; x++)
      r[x] = i[x<<1];
  }
}

/* Subsample an array, equivalent to res = img[:,::2,::2]
*/
void _subsample2_layers_f( float_layers* img, float_layers* res ) {
  const int n_layers = res->tz;
  assert( img->tz==n_layers );
  
  int l;
  #if defined(USE_OPENMP)
  #pragma omp parallel for
  #endif
  for(l=0; l<n_layers; l++) {
    float_image tmp = {img->pixels,img->tx,img->ty};
    float_image rmp = {res->pixels,res->tx,res->ty};
    _subsample2_f( &tmp, &rmp );
  }
}



 /*****************************************************\
 |                   Local pooling                     |
 \*****************************************************/


/* Max-pool in 2x2 px non-overlapping cells
*/
void _maxpool2_layers_f( float_layers* img, float_layers* res ) {
  const int n_layers = res->tz;
  assert( img->tz==n_layers );
  const int tx = res->tx;
  const int ty = res->ty;
  assert( (img->tx)/2 == tx );
  assert( (img->ty)/2 == ty );
  
  int l;
  #if defined(USE_OPENMP)
  #pragma omp parallel for
  #endif
  for(l=0; l<n_layers; l++) {
    int x,y;
    for(y=0; y<ty; y++) {
      float* i = img->pixels + (l*img->ty + (2*y))*img->tx ;
      float* j = i + img->tx;
      float* r = res->pixels + (l*ty + y)*tx;
      for(x=0; x<tx; x++,i+=2,j+=2) {
        float mi = MAX(i[0],i[1]);
        float mj = MAX(j[0],j[1]);
        r[x] = MAX(mi,mj);
      }
    }
  }
}


/* average-pool in 2x2 px non-overlapping cells
*/
void _avgpool2_layers_f( float_layers* img, float_layers* res ) {
  const int n_layers = res->tz;
  assert( img->tz==n_layers );
  const int tx = res->tx;
  const int ty = res->ty;
  assert( (img->tx)/2 == tx );
  assert( (img->ty)/2 == ty );
  
  int l;
  #if defined(USE_OPENMP)
  #pragma omp parallel for
  #endif
  for(l=0; l<n_layers; l++) {
    int x,y;
    for(y=0; y<ty; y++) {
      float* i = img->pixels + (l*img->ty + (2*y))*img->tx ;
      float* j = i + img->tx;
      float* r = res->pixels + (l*ty + y)*tx;
      for(x=0; x<tx; x++,i+=2,j+=2) {
        r[x] = 0.25*(i[0] + i[1] + j[0] + j[1]);
      }
    }
  }
}























































