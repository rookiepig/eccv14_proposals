// -*- c++ -*- 

%module toolbox;

%{
#include "conv.h"
#include "maxfilter.h"
%}

%include <common/numpy_image.swg>

// By default, functions release Python's global lock

%exception {
  Py_BEGIN_ALLOW_THREADS
  $action
  Py_END_ALLOW_THREADS
}

%pythoncode %{
from numpy import *
import pdb
from time import time
%}


/**********************************************************************
* convolution / smoothing
*/

%include "conv.h"


%pythoncode %{

def exec_func( img, funcs, inplace, *args ):
  assert img.ndim in (2,3)
  key = (str(img.dtype),img.ndim==3)
  assert key in funcs, 'error: not implemented for dtype='+str(img.dtype)+(key[1] and ' with layers' or '')
  
  res = inplace if type(inplace)==ndarray else (img if inplace else empty_like(img))
  funcs[key]( img, *(args+(res,)) )
  return res

def smooth_121( img, inplace=False ):
  funcs = {('uint8',0)    : _smooth_121_B,
           ('float32',0)  : _smooth_121_f }
  return exec_func( img, funcs, inplace )

def smooth_gaussian( img, sigma, inplace=False ):
  funcs = {('uint8',0)    : _smooth_gaussian_B,
           ('float32',0)  : _smooth_gaussian_f,
           ('uint8',1)    : _smooth_gaussian_layers_B,
           ('float32',1)  : _smooth_gaussian_layers_f }
  return exec_func( img, funcs, inplace, sigma )

gaussian_blur = smooth_gaussian


def symmetric_convolution( img, half_msk, inplace=False ):
  half_msk = tuple(half_msk)
  if len(half_msk)==2:
    funcs = {('float32',0)  : _sym_filter3_f}
    return exec_func( img, funcs, inplace, half_msk )
  elif len(half_msk)==3:
    funcs = {('float32',0)  : _sym_filter5_f}
    return exec_func( img, funcs, inplace, half_msk )
  elif len(half_msk)==4:
    funcs = {('float32',0)  : _sym_filter7_f}
    return exec_func( img, funcs, inplace, half_msk )
  else:
    assert False, "error: half_mask is either too big or too small"

%}


/**********************************************************************
* max pooling / sub-sampling
*/

%include "maxfilter.h"

%pythoncode %{

def max_filter_3( img, inplace=0, ori='s' ): 
  funcs = { ('uint8',0):  _max_filter_3_B,
            ('int32',0):  _max_filter_3_i,
            ('float32',0):_max_filter_3_f,
            ('float32',1):_max_filter_3_layers_f }
  return exec_func( img, funcs, inplace, ori )

def min_filter_3( img, inplace=0, ori='s' ): 
  funcs = { ('uint8',0):  _min_filter_3_B,
            ('int32',0):  _min_filter_3_i,
            ('float32',0):_min_filter_3_f,
            ('float32',1):_min_filter_3_layers_f }
  return exec_func( img, funcs, inplace, ori )

def max_filter( img, niter=1, inplace=0, ori='s' ):
  """Compute the maximum filter over an image.
  Params:
    niter: radius of the maximum filter (>=1)
    inplace: 0 or 1
    ori: orientation 's' (square), 'h' (horizontal) or 'v' (vertical)
  """
  res = img
  for i in range(niter):
    res = max_filter_3( res, inplace=inplace+i, ori=ori) # always inplace for i>0
  return res

def min_filter( img, niter=1, inplace=0, ori='s' ):
  """Compute the minimum filter over an image.
  Params:
    niter: radius of the minimum filter (>=1)
    inplace: 0 or 1
    ori: orientation 's' (square), 'h' (horizontal) or 'v' (vertical)
  """
  res = img
  for i in range(niter):
    res = min_filter_3( res, inplace=inplace+i, ori=ori) # always inplace for i>0
  return res


def opening( img, niter=1, inplace=0, ori='s' ):
  img = min_filter( img, niter=niter, inplace=inplace, ori=ori )
  img = max_filter( img, niter=niter, inplace=inplace, ori=ori )
  return img


def closing( img, niter=1, inplace=0, ori='s' ):
  img = max_filter( img, niter=niter, inplace=inplace, ori=ori )
  img = min_filter( img, niter=niter, inplace=inplace, ori=ori )
  return img


def empty_like_half( img, add=0 ):
  ty, tx = img.shape[-2:]
  if img.ndim==2:
    return empty(((ty+add)/2,(tx+add)/2),img.dtype)
  else:
    return empty((img.shape[0],(ty+add)/2,(tx+add)/2),img.dtype)


def subsample2( img ):
  res = empty_like_half( img, add=1)
  funcs = { ('float32',1):_subsample2_layers_f }
  return exec_func( img, funcs, inplace=res )


def maxpool2( img ):
  res = empty_like_half(img)
  funcs = { ('float32',1):_maxpool2_layers_f }
  return exec_func( img, funcs, inplace=res )

def avgpool2( img ):
  res = empty_like_half(img)
  funcs = { ('float32',1):_avgpool2_layers_f }
  return exec_func( img, funcs, inplace=res )


def non_max_suppr( img, niter=1, edges=0 ):
  if edges:
    resh = max_filter( img, niter=niter, inplace=0, ori='h' )
    resh[img!=resh] = 0
    resv = max_filter( img, niter=niter, inplace=0, ori='v' )
    resv[img!=resv] = 0
    return maximum(resh,resv)
  else:
    maxf = max_filter( img, niter=niter, inplace=0 )
    maxf[img!=maxf] = 0
    return maxf

%}

