#pragma once
#include "numpy_image.h"

/* propagate the minimum distance along time
  matches = N x 6 = [(x0,y0, x1,y1, cost, useless)]
*/
float temporal_sweep( float_image* matches, 
                      float_image* dmap0, int_image* label0,
                      float_image* dmap1, int_image* label1 );




/* Compare two color distribution. The result is proportional to their similarity.
   seeds = [(x,y,L,a,b, nb, VL, Va, Vb), ...]
*/
void compare_colors( csr_matrix* ngh, const float_image* seeds, bool use_nb, int n_thread );
