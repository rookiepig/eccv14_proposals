// -*- c++ -*- 

%module toolbox;

%{
#include "weighted_dist_trf.h"
#include "nn_field.h"
#include "temporal_dist_trf.h"
%}

%nodefaultctor;
%nodefaultdtor;
%include "common/numpy_image.h" // has to be BEFORE numpy_image.swg
%clearnodefaultctor;
%clearnodefaultdtor;

%include <common/numpy_image.swg>

// By default, functions release Python's global lock

%exception {
  Py_BEGIN_ALLOW_THREADS
  $action
  Py_END_ALLOW_THREADS
}

%pythoncode %{
from numpy import *
import pdb
from time import time
from scipy import sparse
%}

/**********************************************************************
* distance transform
*/

%newobject prepare_approx_dt; // so that adt is owned by python
%newobject prepare_approx_color;

%include "weighted_dist_trf.h"

%extend approx_dt_t {
  ~approx_dt_t() {free_approx_dt($self);}
}


%pythoncode %{

def dt_params(max_iter=40, min_change=1.0):
  params = dt_params_t()
  params.max_iter = max_iter
  params.min_change = min_change
  return params


def distance_transform( seed, cost=None, shape=None, labels=None, thr=None, output_bbox=0, **kwargs ):
  """Compute the distance map over a specified seed placement and cost function. 
  
  Parameters
  ----------
  seed : array_like or tuple of ints or tuple of list of ints
    Position of the initial pixels with 0 cost. If seed is an array, it is 
    interpreted as 0 or non 0 pixels if dtype!=float32.
  cost : scalar or array_like
    If cost is an integer, it is interpreted as constant cost.
  shape : tuple of int
    Has to ge supplied when shape cannot be deduced from the parameters.
  labels : array_like or None
    seed labels, which will be propagated along with the distance map.
  max_iter : integer
  min_change : float
    Minimum change between two iterations for the algorithms to stop.
  thr: float or None
    don't compute the distance transform beyond a maximum distance (faster).
    seed must be the (y,x) position of the seed point in this case.
  """
  if shape==None:
    if type(cost)==ndarray: shape = cost.shape
    elif type(seed)==ndarray: shape = seed.shape
    else: assert False, "error: no shape was specified"
  
  if type(cost)!=ndarray:
    o = ones(shape,dtype=float32)
    o *= cost
    cost = o
  
  params = dt_params(**kwargs)
  
  # init seeding of res
  if thr:
    res = empty(shape,dtype=float32)
    assert type(seed)==tuple and len(seed)==2
    bbox = _weighted_distance_transform_thr( ascontiguousarray(cost), params, seed[::-1], thr, res )
    if output_bbox: res = (res, bbox)
    return res
    
  elif type(seed)==ndarray:
    if seed.dtype!=float32:
      res = ascontiguousarray((seed!=0).astype(float32)*3.39615136e+38)
    else:
      res = seed
  else:
    res = empty(shape,dtype=float32)
    res.view(uint8)[:] = 0x7F # almost infinity
    res[seed] = 0
    assert labels==None
  
  _weighted_distance_transform( ascontiguousarray(cost), params, res, labels  )
  return res


def approx_dist_hub( adt, hub ):
  res = empty((adt.ty,adt.tx), dtype=float32)
  if type(hub)==tuple:
    hx, hy = hub
  else:
    gx = adt._global.tx
    hx = int(hub%gx)
    hy = int(hub/gx)
  _approx_dist_hub( adt, hx, hy, res )
  return res


%}


/**********************************************************************
* nearest-neighbor field for distance transform
*/

%include "nn_field.h"


%pythoncode %{

def xy_field( imshape ):
  res = empty(imshape+(2,), dtype=int32)
  _xy_field(res)
  return res


def euclidean_nnfield( seeds, imshape, nn=1, nt=1 ):
  best = empty(imshape+(nn,), dtype=int32)
  dis =  empty(imshape+(nn,), dtype=float32)
  _euclidean_nnfield( seeds, nn, best, dis, nt )
  return best, dis


def select_median_nn( best, nr, vecs, dis, nt=1 ):
  assert 0<nr<=best.shape[-1]
  if nr<best.shape[-1]: 
    _select_median_nn( best, nr, vecs, dis, nt )
  return best[:,:nr], dis[:,:nr]


def dist_trf_seeds( seeds, cost, nn, bdry_mode=0, nt=1, 
                         linkage='s', min_border_size=0, **kwargs ):
  params = dt_params(**kwargs)
  labels = empty(cost.shape, dtype=int32)
  dmap = empty(cost.shape, dtype=float32)
  nnf = empty((len(seeds),nn), dtype=int32)
  dis  = empty((len(seeds),nn), dtype=float32)
  _dist_trf_seeds_fast( seeds, bdry_mode, cost, linkage, min_border_size, params, labels, dmap, nnf, dis, nt )
  return labels, dmap, nnf, dis


def dist_trf_nnfield( seeds, cost, nn, bdry_mode=0, nt=1, subset=None, 
                         linkage='s', min_border_size=0, **kwargs ):
  params = dt_params(**kwargs)
  if subset==None:
    best = empty(cost.shape+(nn,), dtype=int32)
    dis  = empty(cost.shape+(nn,), dtype=float32)
    _dist_trf_nnfield_fast( seeds, bdry_mode, cost, linkage, min_border_size, params, best, dis, nt )
  else:
    best = empty((len(subset),nn), dtype=int32)
    dis  = empty((len(subset),nn), dtype=float32)
    _dist_trf_nnfield_subset( seeds, bdry_mode, cost, linkage, min_border_size, params, subset, best, dis, nt )
  return best, dis


def dist_trf_dmax_field( seeds, cost, dmax=0, bdry_mode=0, nmin=0, nmax=250, nt=1, 
                         linkage='s', min_border_size=0, **kwargs ):
  params = dt_params(**kwargs)
  csr = _dist_trf_dmaxfield_fast( seeds, bdry_mode, dmax, nmin, nmax, cost, linkage, min_border_size, params, nt )
  return tuple_to_csr(csr)


def kernel_interpolate_csr( nnf, vects, nt=1):
  res = empty((nnf.shape[0],2),dtype=float32)
  _kernel_interpolate_csr( nnf, vects, res, nt )
  return res


def random_regions_dist_trf( cost, dmin, dmax, nt=1, **kwargs):
  params = dt_params(**kwargs)
  csr = _random_regions_dist_trf( dmin, dmax, cost, params, nt )
  return tuple_to_csr(csr)


def ngh_labels_to_spmat( n_seed, labels, dmap, linkage='s', min_border_size=0 ):
  csr = _ngh_labels_to_spmat( n_seed, labels, dmap, linkage, min_border_size )
  return tuple_to_csr(csr)


def ngh_seeds_euclidean( seeds, nn, cost, nt=1 ):
  csr = _ngh_seeds_euclidean( seeds, nn, cost, None, None, nt )
  return tuple_to_csr(csr)


def dist_trf_dmax_seeds( seeds, cost, dmax=0, bdry_mode=0, nmin=0, nmax=250, nt=1, 
                         linkage='s', min_border_size=0, **kwargs ):
  params = dt_params(**kwargs)
  labels = empty(cost.shape, dtype=int32)
  csr = _dist_trf_dmax_seeds_fast( labels, seeds, bdry_mode, dmax, nmin, nmax, cost, linkage, min_border_size, params, nt )
  return tuple_to_csr(csr), labels


def fit_nadarayawatson(nnf, seeds, vects, nt=1):
  seedsvects = empty((nnf.shape[0],2), dtype=float32)
  _fit_nadarayawatson(seedsvects, nnf, seeds, vects, nt)
  return seedsvects

def apply_nadarayawatson(seedsvects, newvects, labels, nt=1):
  _apply_nadarayawatson(seedsvects, newvects, labels, nt)


def viz_edge_weights( edges, labels ):
  img = empty(labels.shape, dtype=float32)
  _viz_edge_weights( edges, labels, img )
  return img

%}

/**********************************************************************
* temporal distance transform
*/

%include "temporal_dist_trf.h"

