#pragma once
#include "numpy_image.h"
#include "assert.h"


typedef struct {
  int max_iter;
  float min_change;
} dt_params_t;

void set_default_dt_params( dt_params_t* params );


/* Compute distance map from a given seeds (in res) and a cost map.
  if labels!=NULL:  labels are propagated along with distance map 
                    (for each pixel, we remember the closest seed)
*/
float _weighted_distance_transform( const float_image* cost, const dt_params_t* dt_params, 
                                   float_image* res, int_image* labels );


/* Compute distance map from a given seeds (in res) and a cost map.
   The distance map will only be partially computed, ie. in all areas where
   distance < thr.
*/
float _weighted_distance_transform_thr( const float_image* cost, const dt_params_t* dt_params, 
                                       const int_Tuple2* seed, const float thr, 
                                       float_image* res, int_Tuple4* res_out );

float _weighted_distance_transform_thr_ex( const float_image* cost, const dt_params_t* dt_params, const float thr, 
                                          float_image* res, int_Tuple4* in_out );


/* Find minimum path from a point to another one given a cost map. 
   (x, y) denotes the end point.
    
   The user must provide the distance_map, generally computed with:
      dmap = weighted_distance_transform( cost, params, res )
   and res[:]=0 excpet at res[start point] = 1
   
   Important: The cost must be >0 everywhere, otherwise the path will get stuck.
*/
void weighted_shortest_path( float_image* dmap, int x, int y, int_image* res_out );



typedef struct {
  int tx, ty;
  int step_global, step_local;
  int hexagrid;
  float_layers global;
  float_layers local;
  dt_params_t dt_params;
} approx_dt_t;

/* Prepare a compressed version of many distance maps to allow for fast approximation of any pixel-to-pixel
   distance computation.
   Default values:
    step_local = 4
    step_global = 16
    max_iter = 40
    min_change = 0.1
*/
approx_dt_t* prepare_approx_dt( const float_image* cost, int step_global, int step_local, 
                                int hexagrid, dt_params_t* dt_params );

/* free the memory
*/
void free_approx_dt( approx_dt_t* adt );


/* Fast compute the approximate distance between two image pixels
*/
float approx_dist( const approx_dt_t* approx_dt, int x1, int y1, int x2, int y2 );


/* return the position of nearest hub point
*/
void img_to_nearest_hub( const approx_dt_t* adt, int x, int y, int_Tuple2* res_out );


/* Fast compute the distance map when the seed point is a hub point
    (hx,hy) : hub point in hub coordinates
*/
void _approx_dist_hub( const approx_dt_t* adt, int hx, int hy, float_image* res );

#define BOUND_X_Y() \
  if(x<0) x=0; \
  if(x>=tx) x=tx-1; \
  if(y<0) y=0; \
  if(y>=ty) y=ty-1;

/* return position of hub point
*/
inline int hub_to_img( const approx_dt_t* adt, int x, int y, int_Tuple2* res_out ) {
  if( adt->hexagrid ) {
    int step = adt->step_global/2;
    x = (2*x-1)*step + (y%2)*step;
    y = (y-1)*step;
  }else {
    x *= adt->step_global;
    y *= adt->step_global;
  }
  
  // correct hub position if it is outside the image
  const int tx=adt->tx, ty=adt->ty;
  BOUND_X_Y()
  
  if(res_out) {res_out[0]=x; res_out[1]=y;}
  return x + y*adt->tx;
}
inline int hubn_to_img( const approx_dt_t* adt, int n, int_Tuple2* res_out ) {
  const int gx = adt->global.tx;
  return hub_to_img(adt,n%gx,n/gx,res_out);
}

inline float dist_hub( const approx_dt_t* adt, int h1, int h2 ) {
  assert(0<=h1 && h1<adt->global.tz);
  assert(0<=h2 && h2<adt->global.tz);
  return adt->global.pixels[h1*adt->global.tz + h2];
}

/* Prepare a compressed version of many distance maps to allow for fast approximation of any pixel-to-pixel
   distance computation.
   Default values:
    step_local = 4
    step_global = 16
    int hexagrid = 0
*/
approx_dt_t* prepare_approx_color( const float_cube* lab, int step_global, int step_local, int hexagrid );



/* get maximum distance for this approx dt
*/
float get_dmax_approx_dt( const approx_dt_t* adt );

/* get mean distance for this approx dt
*/
float get_dmean_approx_dt( const approx_dt_t* adt );

/* Normalize one approx_dt structure
*/
void mul_layers_f( float_layers* res, double mul );
void mul_approx_dt( approx_dt_t* res, double mul );

/* Add two approx_dt structure
*/
void add_layers_f( float_layers* res, const float_layers* add );
void add_approx_dt( approx_dt_t* res, const approx_dt_t* add );

/* Apply a sigmoid function (output is rescaled to [0,1])
*/
void sigmoid_layers_f( float_layers* res, float sigoff, float sigmul );
void sigmoid_approx_dt( approx_dt_t* res, float sigoff, float sigmul );























