"""
Contains utilities function for flows: read/write, convert to color, displaying the color coding,
The functions are similar to the Middelbury function (which were written in matlab by Deqing Sun.
Author: Philippe Weinzaepfel
Version: 1.0
Date: 19 November 2012
"""

import numpy as np
"""import matplotlib.pylab as plt"""
import struct
from PIL import Image

# for read/write 
TAG_FLOAT = 202021.25 # tag to check the sanity of the file
TAG_STRING = 'PIEH'   # string containing the tag
MIN_WIDTH = 1
MAX_WIDTH = 99999
MIN_HEIGHT = 1
MAX_HEIGHT = 99999

# for colors
RY = 15
YG = 6
GC = 4
CB = 11
BM = 13
MR = 6

# for flow
UNKNOWN_THRESH = 1e9

def readFlowFile(filename):
    """
    flow_utils.readFlowFile(<FILENAME>) reads a flow file <FILENAME> into a 2-band np.array.

    if <FILENAME> does not exist, an IOError is raised.
    if <FILENAME> does not finish by '.flo' or the tag, the width, the height or the file's size is illegal, an Expcetion is raised.

    ---- PARAMETERS ----
        filename: string containg the name of the file to read a flow

    ---- OUTPUTS ----
        a np.array of dimension (height x width x 2) containing the flow of type 'float32'
    """
        
    # check filename
    if not filename.endswith(".flo"):
        raise Exception("readFlowFile({:s}): filename must finish with '.flo'".format(filename))
    
    # open the file and read it
    with open(filename,'rb') as f:
        # check tag
        tag = struct.unpack('f',f.read(4))[0]
        if tag != TAG_FLOAT:
            raise Exception("flow_utils.readFlowFile({:s}): wrong tag".format(filename))
        # read dimension
        w,h = struct.unpack('ii',f.read(8))
        if w < MIN_WIDTH or w > MAX_WIDTH:
            raise Exception("flow_utils.readFlowFile({:s}: illegal width {:d}".format(filename,w))
        if h < MIN_HEIGHT or h > MAX_HEIGHT:
            raise Exception("flow_utils.readFlowFile({:s}: illegal height {:d}".format(filename,h))
        flow = np.fromfile(f,'float32')
        if not flow.shape == (h*w*2,):
            raise Exception("flow_utils.readFlowFile({:s}: illegal size of the file".format(filename))
        flow.shape = (h,w,2)
        return flow

def writeFlowFile(flow,filename):
    """
    flow_utils.writeFlowFile(flow,<FILENAME>) write flow to the file <FILENAME>.

    if <FILENAME> does not exist, an IOError is raised.
    if <FILENAME> does not finish with '.flo' or the flow has not 2 bands, an Exception is raised.

    ---- PARAMETERS ----
        flow: np.array of dimension (height x width x 2) containing the flow to write
        filename: string containg the name of the file to write a flow
    """
    
    # check filename
    if not filename.endswith(".flo"):
        raise Exception("flow_utils.writeFlowFile(<flow>,{:s}): filename must finish with '.flo'".format(filename))
    
    if not flow.shape[2:] == (2,):
        raise Exception("flow_utils.writeFlowFile(<flow>,{:s}): <flow> must have 2 bands".format(filename))


    # open the file and write it
    with open(filename,'wb') as f:
        # write TAG
        f.write( TAG_STRING )
        # write dimension
        f.write( struct.pack('ii',flow.shape[1],flow.shape[0]) )
        # write the flow
        flow.astype('float32').tofile(f)

def colorTest():
    """
    flow_utils.colorTest(): display an example of image showing the color encoding scheme
    """
    import matplotlib.pylab as plt
    truerange = 1
    h,w = 151,151
    trange = truerange*1.04
    s2 = round(h/2)
    x,y = np.meshgrid(range(w),range(h))
    u = x*trange/s2-trange
    v = y*trange/s2-trange
    img = _computeColor(np.concatenate((u[:,:,np.newaxis],v[:,:,np.newaxis]),2)/trange/np.sqrt(2))
    plt.imshow(img)
    plt.axis('off')
    plt.axhline(round(h/2),color='k')
    plt.axvline(round(w/2),color='k')
    
def flowToColor(flow, maxflow=None, maxmaxflow=None, saturate=False):
    """
    flow_utils.flowToColor(flow): return a color code flow field, normalized based on the maximum l2-norm of the flow
    flow_utils.flowToColor(flow,maxflow): return a color code flow field, normalized by maxflow

    ---- PARAMETERS ----
        flow: flow to display of shape (height x width x 2)
        maxflow (default:None): if given, normalize the flow by its value, otherwise by the flow norm
        maxmaxflow (default:None): if given, normalize the flow by the max of its value and the flow norm

    ---- OUTPUT ----
        an np.array of shape (height x width x 3) of type uint8 containing a color code of the flow
    """
    h,w,n = flow.shape
    # check size of flow
    if not n == 2:
        raise Exception("flow_utils.flowToColor(flow): flow must have 2 bands")
    # compute max flow if needed
    if maxflow is None:
        maxflow = flowMaxNorm(flow)
    if maxmaxflow is not None:
        maxflow = min(maxmaxflow, maxflow)
    # fix unknown flow
    unknown_idx = np.max(np.abs(flow),2)>UNKNOWN_THRESH
    flow[unknown_idx] = 0.0
    # normalize flow
    eps = np.spacing(1) # minimum positive float value to avoid division by 0
    # compute the flow
    img = _computeColor(flow/(maxflow+eps), saturate=saturate)
    # put black pixels in unknown location
    img[ np.tile( unknown_idx[:,:,np.newaxis],[1,1,3]) ] = 0.0 
    return img

def flowMaxNorm(flow):
    """
    flow_utils.flowMaxNorm(flow): return the maximum of the l2-norm of the given flow

    ---- PARAMETERS ----
        flow: the flow
        
    ---- OUTPUT ----
        a float containing the maximum of the l2-norm of the flow
    """
    return np.max( np.sqrt( np.sum( np.square( flow ) , 2) ) )

def flowMaxNormList(flowlist):
    return max([flowMaxNorm(f) for f in flowlist])

def _computeColor(flow, saturate=True):
    """
    flow_utils._computeColor(flow): compute color codes for the flow field flow
    
    ---- PARAMETERS ----
        flow: np.array of dimension (height x width x 2) containing the flow to display

    ---- OUTPUTS ----
        an np.array of dimension (height x width x 3) containing the color conversion of the flow
    """
    # set nan to 0
    nanidx = np.isnan(flow[:,:,0])
    flow[nanidx] = 0.0
    
    # colorwheel
    ncols = RY + YG + GC + CB + BM + MR
    nchans = 3
    colorwheel = np.zeros((ncols,nchans),'uint8')
    col = 0;
    #RY
    colorwheel[:RY,0] = 255
    colorwheel[:RY,1] = [(255*i) // RY for i in range(RY)]
    col += RY
    # YG    
    colorwheel[col:col+YG,0] = [255 - (255*i) // YG for i in range(YG)]
    colorwheel[col:col+YG,1] = 255
    col += YG
    # GC
    colorwheel[col:col+GC,1] = 255
    colorwheel[col:col+GC,2] = [(255*i) // GC for i in range(GC)]
    col += GC
    # CB
    colorwheel[col:col+CB,1] = [255 - (255*i) // CB for i in range(CB)]
    colorwheel[col:col+CB,2] = 255
    col += CB
    # BM
    colorwheel[col:col+BM,0] = [(255*i) // BM for i in range(BM)]
    colorwheel[col:col+BM,2] = 255
    col += BM
    # MR
    colorwheel[col:col+MR,0] = 255
    colorwheel[col:col+MR,2] = [255 - (255*i) // MR for i in range(MR)]

    # compute utility variables
    rad = np.sqrt( np.sum( np.square(flow) , 2) ) # magnitude
    a = np.arctan2( -flow[:,:,1] , -flow[:,:,0]) / np.pi # angle
    fk = (a+1)/2 * (ncols-1) # map [-1,1] to [0,ncols-1]
    k0 = np.floor(fk).astype('int')
    k1 = k0+1
    k1[k1==ncols] = 0
    f = fk-k0

    if not saturate:
        rad = np.minimum(rad,1)

    # compute the image
    img = np.zeros( (flow.shape[0],flow.shape[1],nchans), 'uint8' )
    for i in range(nchans):
        tmp = colorwheel[:,i].astype('float')
        col0 = tmp[k0]/255
        col1 = tmp[k1]/255
        col = (1-f)*col0 + f*col1
        idx = (rad <= 1)
        col[idx] = 1-rad[idx]*(1-col[idx]) # increase saturation with radius
        col[~idx] *= 0.75 # out of range
        img[:,:,i] = (255*col*(1-nanidx.astype('float'))).astype('uint8')

    return img

def endpointError(flow,gt):
    return np.sqrt( np.sum( np.square( flow-gt), 2) )

def speedFlow(flow):
    return np.sqrt( np.sum( np.square( flow), 2) )

def angularError(flow,gt):
    cosinus = (1 + flow[:,:,0]*gt[:,:,0] + flow[:,:,1]*gt[:,:,1])/( np.sqrt(1+np.sum(np.square(flow),2))*np.sqrt(1+np.sum(np.square(gt),2)))
    # maybe, approximation can lead to result just over 1 or below -1
    cosinus = np.maximum( -1.0, np.minimum( cosinus, 1.0) ) 
    return 180/np.pi*np.arccos(cosinus)

def save_float_png(filename, array):
    """ save a float32 2-dimensional numpy array as a png file"""
    if array.dtype == np.float64:
        array = array.astype(np.float32)
    assert array.dtype == np.float32 and array.ndim == 2 and filename.endswith(".png")
    Image.fromarray( array.view(np.uint8).reshape(array.shape+(4,)) ).save(filename)

def load_float_png(filename):
    """ load a float32 2-dimensional numpy array from a png file"""
    x = np.array( Image.open(filename) )
    return x.reshape(x.shape[0], -1).view(np.float32)
    
def image_warp(im, flow):
    h,w,_ = flow.shape
    yy, xx = np.mgrid[:h,:w]
    xx += flow[:,:,0]
    yy += flow[:,:,1]
    x = np.floor(xx).astype(np.int32)
    y = np.floor(yy).astype(np.int32)
    dx = xx-x
    dy = yy-y
    dx = np.concatenate((dx[:,:,None],dx[:,:,None],dx[:,:,None]), axis=2)
    dy = np.concatenate((dy[:,:,None],dy[:,:,None],dy[:,:,None]), axis=2)
    mask = (xx>=0) * (xx<=w-1) * (yy>=0) * (yy<=h-1)
    x1 = np.maximum(0, np.minimum(x, w-1))
    x2 = np.maximum(0, np.minimum(x+1, w-1))
    y1 = np.maximum(0, np.minimum(y, h-1))
    y2 = np.maximum(0, np.minimum(y+1, h-1))
    dst = im[y1,x1,:]*(1-dx)*(1-dy) + im[y1,x2,:]*dx*(1-dy) + im[y2,x1,:]*(1-dx)*dy  + im[y2,x2,:]*dx*dy;
    return dst, mask
    
def count_for_occlusion(flow):
    h,w,_ = flow.shape
    res = -np.ones((h,w), dtype=np.int32)
    occl = np.zeros((h,w), dtype=np.bool)
    yy,xx = np.mgrid[:h,:w]
    yy = np.round(yy+flow[:,:,1])
    xx = np.round(xx+flow[:,:,0])
    occl = (yy<0)+(yy>h-1)+(xx<0)+(xx>w-1)
    for j in range(h):
        for i in range(w):
            if occl[j,i]:
                continue
            if res[yy[j,i],xx[j,i]] == -1:
                res[yy[j,i],xx[j,i]] = j*w+i
            else:
                k = res[yy[j,i],xx[j,i]]
                occl[k//w,k%w] = True
                occl[j,i] = True
    return occl
