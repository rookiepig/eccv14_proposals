#include "SLIC.h"
#include "std.h"
#include <assert.h>

#include <vector>
//#include <algorithm>
using namespace std;

//#include <cfloat>
//#include <cmath>
//#include <iostream>
//#include <fstream>

#define DBL_MAX 1e38

struct seed_t {
  float x,y,l,a,b;
  seed_t(){}
  seed_t(float f):x(f),y(f),l(f),a(f),b(f){}
  seed_t& operator *= (float f) {
    l *= f;
    a *= f;
    b *= f;
    x *= f;
    y *= f;
    return *this;
  }
};


void DetectLabEdges( float_layers* lab, vector<float>& edges ) {
  const int width = lab->tx;
  const int height = lab->ty;
  const int sz = width*height;
  
  float* lvec = lab->pixels; 
  float* avec = lab->pixels + sz; 
  float* bvec = lab->pixels + 2*sz;
  
  edges.resize(sz,0);
  
  for( int j = 1; j < height-1; j++ )
  {
      for( int k = 1; k < width-1; k++ )
      {
          int i = j*width+k;

          float dx = (lvec[i-1]-lvec[i+1])*(lvec[i-1]-lvec[i+1]) +
                      (avec[i-1]-avec[i+1])*(avec[i-1]-avec[i+1]) +
                      (bvec[i-1]-bvec[i+1])*(bvec[i-1]-bvec[i+1]);

          float dy = (lvec[i-width]-lvec[i+width])*(lvec[i-width]-lvec[i+width]) +
                      (avec[i-width]-avec[i+width])*(avec[i-width]-avec[i+width]) +
                      (bvec[i-width]-bvec[i+width])*(bvec[i-width]-bvec[i+width]);

          //edges[i] = fabs(dx) + fabs(dy);
          edges[i] = dx*dx + dy*dy;
      }
  }
}

//===========================================================================
///    PerturbSeeds
//===========================================================================
void PerturbSeeds( const float_layers* lab, vector<seed_t>& kseeds, const vector<float>& edges ) {
    const int m_width = lab->tx;
    const int m_height = lab->ty;
    const int sz = m_width*m_height;
    
    float* m_lvec = lab->pixels; 
    float* m_avec = lab->pixels + sz; 
    float* m_bvec = lab->pixels + 2*sz;
    
    const int dx8[8] = {-1, -1,  0,  1, 1, 1, 0, -1};
    const int dy8[8] = { 0, -1, -1, -1, 0, 1, 1,  1};
    
    int numseeds = kseeds.size();
    
    for( int n = 0; n < numseeds; n++ )
    {
        int ox = kseeds[n].x;//original x
        int oy = kseeds[n].y;//original y
        int oind = oy*m_width + ox;

        int storeind = oind;
        for( int i = 0; i < 8; i++ )
        {
            int nx = ox+dx8[i];//new x
            int ny = oy+dy8[i];//new y

            if( nx >= 0 && nx < m_width && ny >= 0 && ny < m_height)
            {
                int nind = ny*m_width + nx;
                if( edges[nind] < edges[storeind])
                {
                    storeind = nind;
                }
            }
        }
        if(storeind != oind)
        {
            kseeds[n].x = storeind%m_width;
            kseeds[n].y = storeind/m_width;
            kseeds[n].l = m_lvec[storeind];
            kseeds[n].a = m_avec[storeind];
            kseeds[n].b = m_bvec[storeind];
        }
    }
}


//===========================================================================
///    GetLABXYSeeds_ForGivenStepSize
///
/// The k seed values are taken as uniform spatial pixel samples.
//===========================================================================
void GetLABXYSeeds_ForGivenStepSize( float_layers* lab, vector<seed_t>& kseeds, int STEP, bool hexgrid ) {
  const int m_width = lab->tx;
  const int m_height = lab->ty;
  const int sz = m_width*m_height;
  
  float* m_lvec = lab->pixels; 
  float* m_avec = lab->pixels + sz; 
  float* m_bvec = lab->pixels + 2*sz;
  
  int xstrips = (0.5+float(m_width)/float(STEP));
  int ystrips = (0.5+float(m_height)/float(STEP));

  int xerr = m_width  - STEP*xstrips; if(xerr < 0){xstrips--;xerr = m_width - STEP*xstrips;}
  int yerr = m_height - STEP*ystrips; if(yerr < 0){ystrips--;yerr = m_height- STEP*ystrips;}
  
  float xerrperstrip = float(xerr)/float(xstrips);
  float yerrperstrip = float(yerr)/float(ystrips);
  
  int xoff = STEP/2;
  int yoff = STEP/2;
  int numseeds = xstrips*ystrips;
  kseeds.resize(numseeds);
  
  int n=0;
  for( int y = 0; y < ystrips; y++ )
  {
      int ye = y*yerrperstrip;
      for( int x = 0; x < xstrips; x++ )
      {
          int xe = x*xerrperstrip;
          int seedx = (x*STEP+xoff+xe);
          if(hexgrid){ seedx = x*STEP+(xoff>>(y&0x1))+xe; seedx = min(m_width-1,seedx); }//for hex grid sampling
          int seedy = (y*STEP+yoff+ye);
          int i = seedy*m_width + seedx;
          
          kseeds[n].l = m_lvec[i];
          kseeds[n].a = m_avec[i];
          kseeds[n].b = m_bvec[i];
          kseeds[n].x = seedx;
          kseeds[n].y = seedy;
          n++;
      }
  }
}



//===========================================================================
///    PerformSuperpixelSLIC
///
///    Performs k mean segmentation. It is fast because it looks locally, not
/// over the entire image.
//===========================================================================
void PerformSuperpixelSLIC( float_layers* lab, vector<seed_t>& kseeds, int_image* labels, 
                            int STEP, float M, int n_iter, float extend_window ) {
  const int m_width = lab->tx;
  const int m_height = lab->ty;
  const int sz = m_width*m_height;
  
  float* m_lvec = lab->pixels; 
  float* m_avec = lab->pixels + sz; 
  float* m_bvec = lab->pixels + 2*sz;
  int* klabels = labels->pixels;
  
  const int numk = kseeds.size();
  //----------------
  int offset = int(STEP*(1+extend_window));
  if(STEP < 8) offset = STEP*1.5; //to prevent a crash due to a very small step size
  //----------------
  
  vector<float> clustersize(numk, 0);
  
  vector<seed_t> sigma(numk, 0);
  vector<float> distvec(sz, DBL_MAX);
  
  float invwt = 1.0/((STEP/M)*(STEP/M));
  
  int x1, y1, x2, y2;
  float l, a, b;
  float dist;
  float distxy;
  for( int itr = 0; itr < n_iter; itr++ )
  {
      distvec.assign(sz, DBL_MAX);
      for( int n = 0; n < numk; n++ )
      {
          y1 = max(0.0f,               kseeds[n].y-offset);
          y2 = min((float)m_height,    kseeds[n].y+offset);
          x1 = max(0.0f,               kseeds[n].x-offset);
          x2 = min((float)m_width,     kseeds[n].x+offset);
          
          for( int y = y1; y < y2; y++ )
          {
              for( int x = x1; x < x2; x++ )
              {
                  int i = y*m_width + x;
                  
                  l = m_lvec[i];
                  a = m_avec[i];
                  b = m_bvec[i];
                  
                  dist =          (l - kseeds[n].l)*(l - kseeds[n].l) +
                                  (a - kseeds[n].a)*(a - kseeds[n].a) +
                                  (b - kseeds[n].b)*(b - kseeds[n].b);
                  
                  distxy =        (x - kseeds[n].x)*(x - kseeds[n].x) +
                                  (y - kseeds[n].y)*(y - kseeds[n].y);
                  
                  //------------------------------------------------------------------------
                  dist += distxy*invwt;
                  //dist = sqrt(dist) + sqrt(distxy*invwt);//this is more exact
                  //------------------------------------------------------------------------
                  if( dist < distvec[i] )
                  {
                      distvec[i] = dist;
                      klabels[i]  = n;
                  }
              }
          }
      }
      
      //-----------------------------------------------------------------
      // Recalculate the centroid and store in the seed values
      //-----------------------------------------------------------------
      //instead of reassigning memory on each iteration, just reset.
  
      sigma.assign(numk, 0);
      clustersize.assign(numk, 0);
      
      {int ind = 0;
      for(int r=0; r<m_height; r++)
          for(int c=0; c<m_width; c++) {
              seed_t& sig = sigma[klabels[ind]];
              sig.l += m_lvec[ind];
              sig.a += m_avec[ind];
              sig.b += m_bvec[ind];
              sig.x += c;
              sig.y += r;
              clustersize[klabels[ind]] += 1.0;
              ind++;
          }
      }
      
      for( int k = 0; k < numk; k++ ) {
          float inv = 1.0/MAX(1,clustersize[k]);
          kseeds[k] = (sigma[k] *= inv);
      }
  }
}


/* Compute SLIC superpixels.
*/
int _compute_SLIC( float_layers* lab, int_image* labels, 
                    int superpixelsize, float compactness, bool perturbseeds, 
                    int n_iter, float extend_window,
                    bool output_seeds, float_image* seeds ) {
  
  // ini step size
  const int STEP = sqrt(float(superpixelsize))+0.5;
  
  // init labels
  const int sz = lab->tx * lab->ty;
  memset(labels->pixels,0xFF,sz*sizeof(int));
  
  //--------------------------------------------------
  vector<seed_t> kseeds;
  GetLABXYSeeds_ForGivenStepSize( lab, kseeds, STEP, false);
  
  // perturb seeds if necessary
  if(perturbseeds)  {
    vector<float> edgemag(0);
    DetectLabEdges( lab, edgemag );
    PerturbSeeds( lab, kseeds, edgemag );
  }
  
  PerformSuperpixelSLIC( lab, kseeds, labels, STEP, compactness, n_iter, extend_window);
  int numlabels = kseeds.size();
  
  if( output_seeds ) {
    assert(!seeds->pixels);
    *seeds = (float_image){new float[5*kseeds.size()],5,(int)kseeds.size()};
    memcpy(seeds->pixels,kseeds.data(),kseeds.size()*sizeof(seed_t));
  }
  
  return numlabels;
}



//===========================================================================
///    EnforceLabelConnectivity
///
///        1. finding an adjacent label for each new component at the start
///        2. if a certain component is too small, assigning the previously found
///            adjacent label to this component, and not incrementing the label.
//===========================================================================
int enforce_contiguous_regions( int_image* klabels, int_image* res, int K) { //the number of superpixels desired by the user
    const int width = klabels->tx;
    const int height = klabels->ty;
    const int sz = width*height;
    const int SUPSZ = sz/K;
    
    const int* labels = klabels->pixels;
    int* nlabels = res->pixels;
    memset(nlabels,0xFF,sz*sizeof(int));
    
    const int dx4[4] = {-1,  0,  1,  0};
    const int dy4[4] = { 0, -1,  0,  1};
    
    int label(0);
    int* xvec = new int[sz];
    int* yvec = new int[sz];
    int oindex(0);
    int adjlabel(0);//adjacent label
    for( int j = 0; j < height; j++ )
    {
        for( int k = 0; k < width; k++ )
        {
            if( 0 > nlabels[oindex] )
            {
                nlabels[oindex] = label;
                //--------------------
                // Start a new segment
                //--------------------
                xvec[0] = k;
                yvec[0] = j;
                //-------------------------------------------------------
                // Quickly find an adjacent label for use later if needed
                //-------------------------------------------------------
                
                {for( int n = 0; n < 4; n++ )
                {
                    int x = xvec[0] + dx4[n];
                    int y = yvec[0] + dy4[n];
                    if( (x >= 0 && x < width) && (y >= 0 && y < height) )
                    {
                        int nindex = y*width + x;
                        if(nlabels[nindex] >= 0) adjlabel = nlabels[nindex];
                    }
                }}
                int count(1);
                for( int c = 0; c < count; c++ )
                {
                    for( int n = 0; n < 4; n++ )
                    {
                        int x = xvec[c] + dx4[n];
                        int y = yvec[c] + dy4[n];
                        if( (x >= 0 && x < width) && (y >= 0 && y < height) )
                        {
                            int nindex = y*width + x;
                            if( 0 > nlabels[nindex] && labels[oindex] == labels[nindex] )
                            {
                                xvec[count] = x;
                                yvec[count] = y;
                                nlabels[nindex] = label;
                                count++;
                            }
                        }
                    }
                }
                //-------------------------------------------------------
                // If segment size is less then a limit, assign an
                // adjacent label found before, and decrement label count.
                //-------------------------------------------------------
                
                if(count <= SUPSZ >> 2)
                {
                    for( int c = 0; c < count; c++ )
                    {
                        int ind = yvec[c]*width+xvec[c];
                        nlabels[ind] = adjlabel;
                    }
                    label--;
                }
                label++;
            }
            oindex++;
        }
    }
    
    if(xvec) delete [] xvec;
    if(yvec) delete [] yvec;
    
    return label;
}


/* recompute seeds after enforce_contiguous_regions
   seeds = [(x, y, L, a, b, nb) + optional (Vx, Vy, VL, Va, Vb)]
   mask => which pixels are ok to look at
*/
void _recompute_seeds( float_cube* lab, int_image* labels, float_image* seeds, UBYTE_image* mask ) {
  ASSERT_SAME_SIZE(lab,labels);
  assert(lab->tz==3);
  const int nl = seeds->ty;
  const int sx = seeds->tx;
  assert(sx==6 || sx==9);
  const int width = lab->tx;
  const int height = lab->ty;
  const UBYTE* msk = mask ? mask->pixels : NULL;
  
  // init seeds
  memset(seeds->pixels,0,sx*nl*sizeof(float));
  
  for(int j=0; j<height; j++ )
    for( int i=0; i<width; i++) {
      const int p = i + j*width;
      const float* color = lab->pixels + (p)*3;
      const int l = labels->pixels[p];
      float* seed = seeds->pixels + l*sx;
      
      const float w = msk? msk[p] + 1e-16 : 1;
      seed[5] += w;
      seed[0] += w*i;
      seed[1] += w*j;
      seed[2] += w*color[0];
      seed[3] += w*color[1];
      seed[4] += w*color[2];
      if( sx==9 ) {
      seed[6] += w*color[0]*color[0];
      seed[7] += w*color[1]*color[1];
      seed[8] += w*color[2]*color[2];
      }
    }
  
  for(int l=0; l<nl; l++) {
    float* seed = seeds->pixels + l*sx;
    seed[5] += 1e-16;
    seed[0] /= seed[5];
    seed[1] /= seed[5];
    seed[2] /= seed[5];
    seed[3] /= seed[5];
    seed[4] /= seed[5];
    if( sx == 9 ) { // variance
    seed[6] = MAX(0,seed[6]/seed[5] - seed[2]*seed[2]);
    seed[7] = MAX(0,seed[7]/seed[5] - seed[3]*seed[3]);
    seed[8] = MAX(0,seed[8]/seed[5] - seed[4]*seed[4]);
    }
  }
}



void draw_SLIC_contours( UBYTE_cube* rgb, int_image* labels ) {
  ASSERT_SAME_SIZE(rgb,labels);
  
  const int dx8[8] = {-1, -1,  0,  1, 1, 1, 0, -1};
  const int dy8[8] = { 0, -1, -1, -1, 0, 1, 1,  1};
  
  const int width = rgb->tx;
  const int height = rgb->ty;
  int sz = width*height;
  int* klabels = labels->pixels;
  
  vector<bool> istaken(sz, false);
  vector<int> contourx(sz);
  vector<int> contoury(sz);
  int mainindex=0,cind=0;
  
  for( int j = 0; j < height; j++ )
  {
      for( int k = 0; k < width; k++ )
      {
          int np(0);
          for( int i = 0; i < 8; i++ )
          {
              int x = k + dx8[i];
              int y = j + dy8[i];

              if( (x >= 0 && x < width) && (y >= 0 && y < height) )
              {
                  int index = y*width + x;

                  //if( false == istaken[index] )//comment this to obtain internal contours
                  {
                      if( klabels[mainindex] != klabels[index] ) np++;
                  }
              }
          }
          if( np > 1 )
          {
              contourx[cind] = k;
              contoury[cind] = j;
              istaken[mainindex] = true;
              cind++;
          }
          mainindex++;
      }
  }
  
  int numboundpix = cind;
  for( int j = 0; j < numboundpix; j++ )
  {
      int ii = contoury[j]*width + contourx[j];
      memset(rgb->pixels + 3*ii, 0xff, 3 );
      
      for( int n = 0; n < 8; n++ )
      {
          int x = contourx[j] + dx8[n];
          int y = contoury[j] + dy8[n];
          if( (x >= 0 && x < width) && (y >= 0 && y < height) )
          {
              int ind = y*width + x;
              if(!istaken[ind]) 
                memset(rgb->pixels + 3*ind, 0x00, 3 );
          }
      }
  }
}








