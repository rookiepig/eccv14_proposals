#include "temporal_hiera_cluster_boost.h"
#include "hiera_cluster_shared.h"
#include "std.h"
#include "disjoint-set.h"
#include <boost/multi_index_container.hpp>
#include <boost/multi_index/ordered_index.hpp>
#include <boost/multi_index/hashed_index.hpp>
#include <boost/multi_index/sequenced_index.hpp>
#include <boost/multi_index/identity.hpp>
#include <boost/multi_index/member.hpp>
using namespace ::boost;
using namespace ::boost::multi_index;
#include <vector>
using namespace std;

namespace thcb {

typedef vector<int> frameset_t;

static inline void union_set( frameset_t& res, const frameset_t& add ) {
  unsigned int k=0;
  for(unsigned int i=0; i<res.size() && k<add.size(); i++) {
    while( add[k] < res[i] )  {
      res.insert(res.begin()+(i++),add[k++]); // add it
      if(k>=add.size()) return;
    }
    if( add[k] == res[i] )  k++; 
  }
  while( k < add.size() )
    res.emplace_back(add[k++]);
}

static inline unsigned int size_inter_set( const frameset_t& set0, const frameset_t& set1 ) {
  unsigned int i=0,j=0,nb=0;
  while( i<set0.size() && j<set1.size() ) {
    while( set0[i] < set1[j] ) {i++; if(i>=set0.size()) return nb;}
    while( set0[i] > set1[j] ) {j++; if(j>=set1.size()) return nb;}
    if( set0[i] == set1[j] ) {nb++; i++; j++;}
  }
  return nb;
}

struct cluster_t {
  frameset_t frames;  // set of frames in which the cluster appears
  void merge( cluster_t& add ) {
    union_set(frames,add.frames);
  }
};

static inline long key( long i, long j ) {
  if(j>i) SWAP(i,j,long);   // always i<j 
  return (j<<32) + i;
}

/* Structure to describe a connection between two clusters.
*/
struct edge_t {
  int node1, node2;
  long node12;  // concatenation of node1, node2
  
  float edge_dist; // true edge distance
  float w; // weight
  frameset_t frames;  // frames in which we are connected
  float dist; // penalized distance
  
  static char linkage;
  static float penalty_coef;
  static char penalty_mode;
  
  edge_t(){}
  edge_t(int n1, int n2, float dis, int frame, float w=1)
    :node1(n1),node2(n2),node12(key(n1,n2)),edge_dist(dis),w(w),dist(dis) {
    assert(dist>=0);
    frames.emplace_back(frame);
  }
  
  edge_t& merge( const edge_t& d ) {
    // this edge connect node0 and node1
    // node0 is the merge of (node0, node2) 
    // d is the previous edge between (node2, node1)
    
    switch(linkage) {
    case 's':
      edge_dist = MIN(edge_dist,d.edge_dist);
      break;
    case 'a':
      edge_dist = (w*edge_dist + d.w*d.edge_dist)/(w+d.w);
      w += d.w;
      break;
    case 'm':
      edge_dist = MAX(edge_dist,d.edge_dist);
      break;
    default:  
      assert(!"error: unknown linkage");
    }
    
    // union frames in which the edge appears
    union_set(frames, d.frames);
    
    // you should probably call update_penalty after this
    dist = NAN;
    return *this;
  }
  
  bool update_penalty( const cluster_t& node0, const cluster_t& node1 ) {
    // this edge connect node0 and node1
    
    // compute number of frames missing an edge = len( inter(node0_frames, node1_frames1) - edge_frames )
    // because we know that edge_frames \belongs inter(node0_frames, node1_frames1)
    // then :  penalty = len(inter(node0_frames, node1_frames1))  - len( edge_frames )
    int n_shared = size_inter_set(node0.frames, node1.frames);  // frames in common
    int n_contact = (int)frames.size();  // frames where nodes are in contact
    assert(n_shared>=n_contact);
    
    float old_dist = dist;
    dist = edge_dist;
    switch(penalty_mode) {
    case 0: break;
    case 1:  dist += penalty_coef * (n_shared-n_contact); break;
    case 2:  dist += penalty_coef * pow2(n_shared-n_contact); break;
    case 11: 
      if( n_shared )
        dist = (n_contact*dist + (n_shared-n_contact)*penalty_coef)/n_shared; 
      break;
    case 12:  
      if( n_shared )  
        dist = (pow2(n_contact)*dist + pow2(n_shared-n_contact)*penalty_coef) /
                    (pow2(n_contact)+pow2(n_shared-n_contact)); 
      break;
    default:  assert(!"error: unknown penalty mode");
    }
    assert(dist>=0);
    return old_dist != dist;
  }
};

char edge_t::linkage = '\0';
float edge_t::penalty_coef = 0;
char edge_t::penalty_mode = 0;

#include "union_t.h"
}

/* Perform hierarchical clustering on a set of points, where only some
   pairwise distances are known. 
   
   dist_mat: must be a lower-triangular sparse square CSR matrix
   
   linkage: 's' (minimum), 'm' (maximum) or 'a' (average)
   
   weights: in case of average linkage, initial weights for each pair of elem
   
   res_out: succession of fusions
*/
void _temporal_tree_cluster_boost( const csr_matrix* dist_mat, int_array* _frames, 
                                   char linkage, int penalty_mode, float penalty_coef,
                                   csr_matrix* weights, int verbose, int_image* res_out,
                                   int n_clusters_stop, coo_matrix* edges_out ) {
  assert(dist_mat->nr==dist_mat->nc);
  const int nl = dist_mat->nr;  // number of initial labels
  assert(!weights || (weights->nr==nl && weights->nc==nl && weights->indptr[nl]==dist_mat->indptr[nl]));
  assert(!weights || memcmp(dist_mat->indptr,weights->indptr,(nl+1)*sizeof(int))==0);
  assert(_frames->tx==nl);
  
  // build cluster_frames
  const int* frames = _frames->pixels;
  vector<thcb::cluster_t> cluster_frames(nl);  // frames in which this cluster currently appears
  for(int i=0; i<nl; i++)
    cluster_frames[i].frames.assign(1,frames[i]);
  
  // disjoint-set structure
  int* assign = NEWA(int,nl);
  for(int i=0; i<nl; i++) assign[i]=i;
  int* ranks = NEWAC(int,nl);
  
  // compute the set of initial pairwise distances
  thcb::edge_t::linkage = linkage;
  thcb::edge_t::penalty_mode = penalty_mode;
  thcb::edge_t::penalty_coef = penalty_coef;
  typedef multi_index_container<thcb::edge_t,
    indexed_by<
      ordered_non_unique<member<thcb::edge_t,float,&thcb::edge_t::dist> >,
      hashed_unique<member<thcb::edge_t,long,&thcb::edge_t::node12> >,
      hashed_non_unique<member<thcb::edge_t,int,&thcb::edge_t::node1> >,
      hashed_non_unique<member<thcb::edge_t,int,&thcb::edge_t::node2> >
    > > sp_edge_t;
  #define find_neighbors( hash_node, bi ) \
    {auto sames = hash_node.equal_range(bi);  \
    for(auto it=sames.first; it!=sames.second; ++it)  \
      others.emplace_back(it->node1==bi ? it->node2 : it->node1);}

  sp_edge_t sp_dist;
  
  for(int r=0; r<nl; r++) {
    int ncol = dist_mat->indptr[r+1] - dist_mat->indptr[r];
    if(!ncol) continue;
    
    int* cols = dist_mat->indices + dist_mat->indptr[r];
    float* data = dist_mat->data + dist_mat->indptr[r];
    float* w = weights ? weights->data +  weights->indptr[r]: NULL;
    
    for(int c=0; c<ncol; c++)
      if(cols[c]<r) { // we keep only lower triangle
        thcb::edge_t edge(cols[c], r, data[c], frames[r], w?w[c]:1); 
        if( frames[r] != frames[cols[c]] ) // inter-frames edges
          edge.frames.clear();  // no common frames
        sp_dist.insert(edge);
      }
  }
  assert(sp_dist.size() || !"error: dist_mat is not lower-triangular");
  
  // output tree structure
  vector<thcb::union_t> res;
  // number of clusters = nl-res.size()
  while(int(nl-res.size())>n_clusters_stop && sp_dist.size()>0) {
    if(verbose && res.size()%100==0) {
      printf("\rhierarchical clustering %d%% (%ld clusters remaining)...",100*int(res.size())/nl,nl-res.size());
      fflush(stdout);
    }
    
    // find best distance
    const thcb::edge_t& best = *sp_dist.get<0>().begin();
    int bi=best.node2,bj=best.node1;
    assert(FindRepresentative(assign,bi)==bi);
    assert(FindRepresentative(assign,bj)==bj);
    
    // fusion
    Union(assign,ranks,bi,bj);
    res.emplace_back(bi,bj,best.dist);  // bi,bj are in the original space of elems
    if( FindRepresentative(assign,bi)!=bi )  SWAP(bi,bj,int);  // bi is the new representative
    cluster_frames[bi].merge(cluster_frames[bj]); // merge frames
    
    // recompute new distances with other classes
    vector<int> others;
    find_neighbors( sp_dist.get<2>(), bi );
    find_neighbors( sp_dist.get<2>(), bj );
    find_neighbors( sp_dist.get<3>(), bi );
    find_neighbors( sp_dist.get<3>(), bj );
    auto& hash_edge = sp_dist.get<1>();
    for(unsigned int _o=0; _o<others.size(); _o++) {
      int o = others[_o];  // o = other cluster
      if(o==bi || o==bj) continue;
      assert(FindRepresentative(assign,o)==o);
      
      auto bi_to_o = hash_edge.find(thcb::key(bi,o));
      auto bj_to_o = hash_edge.find(thcb::key(bj,o));
      
      if( bi_to_o!=hash_edge.end() && bj_to_o!=hash_edge.end() ) {
        // o was linked to bi and bj
        thcb::edge_t bi_to_o2 =  *bi_to_o;
        bi_to_o2.merge( *bj_to_o ).update_penalty( cluster_frames[bi], cluster_frames[o] );
        hash_edge.replace( bi_to_o, bi_to_o2 );
        hash_edge.erase( bj_to_o ); // remove obsolete edge
        
      } else if( bj_to_o!=hash_edge.end() ) {
        // o was linked to bj only
        thcb::edge_t bj_to_o2 =  *bj_to_o;
        bj_to_o2.node1 = bi;
        bj_to_o2.node2 = o;
        bj_to_o2.node12 = thcb::key(bi,o);
        bj_to_o2.update_penalty( cluster_frames[bi], cluster_frames[o] );
        hash_edge.replace(bj_to_o, bj_to_o2 ); 
        
      } else if( bi_to_o!=hash_edge.end() ) {
        // o was linked to bi only
        thcb::edge_t bi_to_o2 =  *bi_to_o;
        if(bi_to_o2.update_penalty( cluster_frames[bi], cluster_frames[o] ))
          hash_edge.replace( bi_to_o, bi_to_o2 );
      }
    }
    
    // remove old obsolete distances (with bj)
    hash_edge.erase( hash_edge.find(thcb::key(bi,bj)) );
  }
  if(verbose) printf(" done!\n");
  
  free(ranks);
  free(assign);
  
  res_out->tx = 3;
  res_out->ty = res.size();
  res_out->pixels = NEWA(int,3*res.size());
  memcpy(res_out->pixels,res.data(),3*res.size()*sizeof(int));
  
  if( n_clusters_stop > 0 ) { // we output the edges at current state
    assert(!edges_out->data); 
    int nedges = sp_dist.size();
    edges_out->nr = dist_mat->nr;
    edges_out->nc = dist_mat->nc;
    edges_out->row = NEWA(int, nedges);
    edges_out->col = NEWA(int, nedges);
    edges_out->data = NEWA(float, nedges);
    const auto& list_edges = sp_dist.get<0>();
    int i=0;
    for(auto it=list_edges.begin(); it!=list_edges.end(); ++it,++i) {
      edges_out->row[i] = it->node1;
      edges_out->col[i] = it->node2;
      edges_out->data[i] = it->dist;
    }
    assert(i==nedges);
    edges_out->n_elem = nedges;
  }
}

























