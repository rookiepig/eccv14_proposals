// -*- c++ -*- 

%module toolbox;

%{
#include "hiera_cluster_shared.h"
#include "temporal_hiera_cluster_boost.h"
%}

%include <common/numpy_image.swg>

// By default, functions release Python's global lock

%exception {
  Py_BEGIN_ALLOW_THREADS
  $action
  Py_END_ALLOW_THREADS
}

%pythoncode %{
import numpy as np
from scipy import sparse
%}


/**********************************************************************
* hierarchical clustering
*/

%include "hiera_cluster_shared.h"
%include "temporal_hiera_cluster_boost.h"


%pythoncode %{

def tree_cluster( dist_mat, linkage='a', weights=None, verbose=0 ):
  node_frames = np.zeros(dist_mat.shape[0], dtype=np.int32)
  return temporal_tree_cluster( dist_mat, node_frames, penalty_coef=0, mode=0, 
                           linkage=linkage, weights=weights, verbose=verbose )


def temporal_tree_cluster( dist_mat, node_frames, penalty_coef, mode=1, 
                           linkage='a', weights=None, output_edges_at=0,
                           verbose=0, boost=1 ):
  if boost:
    tree, coo = _temporal_tree_cluster_boost( dist_mat, node_frames, linkage, mode, penalty_coef, weights, verbose, output_edges_at )
    if output_edges_at:
      nr,nc,row,col,data=coo
      tree = (tree,sparse.coo_matrix((data,(row,col)),shape=(nr,nc),dtype=np.float32))
  else:
    assert output_edges_at==0
    tree = _temporal_tree_cluster( dist_mat, node_frames, linkage, mode, penalty_coef, weights, verbose )
  return tree

def tree_assign( n_clusters, n_labels, tree, remap=1 ):
  assign = np.empty(n_labels, dtype=np.int32)
  n_clusters = _tree_assign_n_cluster( n_clusters, tree, assign, remap )
  return n_clusters, assign


class Node:
  def __init__(self, elems=None, left=None, right=None):
    self.left = left
    self.right = right
    if left is None:
      assert right is None and type(elems)==list
      self.elems = elems  # list of labels
    else:
      assert elems is None
      self.elems = left.elems + right.elems
      self.elems.sort() # re-sort, even if pretty useless
  def is_leaf(self):
    return self.left is None
  def __len__(self):
    return len(self.elems)

def get_tree_root( n_labels, tree ):
  # build each leaf
  nodes = []
  for i in range(n_labels):
    nodes.append( Node([i]) )
  
  from utils import disjointset
  label,ranks = disjointset.Prepare(len(nodes))
  for i in range(len(tree)):
    a,b = tree[i,:2]  # union between a and b
    n = Node(left=nodes[a], right=nodes[b])
    disjointset.Union(a,b,label,ranks)
    nodes[label[a]] = n
  
  return n

%}

