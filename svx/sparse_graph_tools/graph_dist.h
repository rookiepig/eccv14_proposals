#include "numpy_image.h"
#include <vector>
using namespace std;

typedef vector<int> list_node_t;

struct node_dist_t {
  int node;
  float dis;
  node_dist_t(){}
  node_dist_t(int i, float f):node(i),dis(f){}
};
typedef vector<node_dist_t> list_node_dist_t;


/* Build a CSR matrix
*/
void build_csr_from_sorted_rows( const vector<list_node_t>& sorted_rows, const int nc, csr_matrix* csr );
void build_csr_from_sorted_rows_weighted( const vector<list_node_dist_t>& sorted_rows, const int nc, csr_matrix* csr );


/* Find nearest neighbour in a weighted directed graph
*/
int _find_nn_graph_arr( csr_matrix* graph, int seed, int nn, int* best, float* dist );
int _find_nn_graph( csr_matrix* graph, int seed, int_array* best, float_array* dist );


/* Find all nodes inside a ball of given radius for a weighted graph.
*/
void _find_dmax_graph_arr( csr_matrix* graph, int seed, float dmax, list_node_dist_t& res );
void find_dmax_graph( csr_matrix* graph, int seed, float dmax, int_array* res_out, float_array* res_out2 );



/* Count the number of neighboring nodes in a given radius (including seed). 
   The matrix must be symmetric !
   If weights!=NULL, then it represents the node weights (instead of 1).
*/
float _count_neighbours_node_arr( const csr_matrix* assoc, int seed, int rad, const float* weights, int* done );
void _count_neighbours( const csr_matrix* assoc, int rad, const float_array* _weights, float_array* res );



/* Find the neighbours in a given hop radius of all graph nodes. 
   The shortest distance to the seed is returned as well.
   The matrix must be symmetric !
*/
void _find_neighbours_node_arr( const csr_matrix* assoc, int seed, int radmin, int radmax, list_node_t& nghs, char* _done);
void _find_neighbours_node( const csr_matrix* assoc, int seed, int radmin, int radmax, int_array* res_out );
void _find_neighbours( const csr_matrix* assoc, int radmin, int radmax, csr_matrix* res_out, int n_thread );



/* Maximum filter in a radius of 1. Edge weights are not used.
   The matrix must be symmetric !
*/
void _max_filter( const csr_matrix* ngh, float_array* current, float_array* res );



/* Find the shortest (weighted) path between two nodes.
   graph matrix must be symmetric !
*/
float _shortest_weighted_path( csr_matrix* graph, int seed, int end, int output_path, int_array* res_out );


/* Compute all shortest distances between all graph nodes
*/
//void _compute_graph_distance_matrix( const csr_matrix* ngh, float_image* res );



/* Compute the weighted shortest distance from a seed node to all graph nodes 
   within a given number of 'hops'.
   Return an image of minimum distance for each node and for each number of 'hops' : res[node,n_hop-1]
   ngh MUST be a sparse SYMMETRIC matrix
*/
void _shortest_weighted_nhops_node( const csr_matrix* graph, int seed, int nmax_hop, float_image* res );
void _shortest_weighted_nhops( const csr_matrix* graph, int radmax, csr_matrix* res_out, int n_thread );





































