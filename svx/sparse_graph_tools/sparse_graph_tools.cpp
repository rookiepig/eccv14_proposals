#include "sparse_graph_tools.h"
#include "std.h"
#include "disjoint-set.h"
#include <queue>
using namespace std;



int find_connected_components(coo_matrix* graph, int_array* res) {
  assert(graph->nr==graph->nc); // matrix is square
  assert(graph->nr==res->tx);  // same length
  
  int n, n_nodes = res->tx;
  int* parents = NEWA(int,n_nodes);
  int* ranks   = res->pixels;
  memset(ranks,0,sizeof(int)*n_nodes);
  
  // Make sets
  for(n=0; n<n_nodes; n++)  
    parents[n]=n; // default: one set for each node
  
  int nedges=graph->n_elem;
  int* row = graph->row;
  int* col = graph->col;
  float* data = graph->data;
  for(n=0; n<nedges; n++) {
    if(!data[n])  continue;
    int i = row[n]; // start node
    int j = col[n]; // end node
    
    // union both associated sets
    Union(parents,ranks,i,j);
  }
  
  // find back the connected components
  int* labels = res->pixels;  // because ranks is useless now
  memset(labels,0xFF,sizeof(int)*n_nodes);  // all are equal to -1 by default
  int n_labels = 0;
  for(n=0; n<n_nodes; n++)  {
    int r = FindRepresentative(parents,n);
    if( labels[r] < 0 )
      labels[r] = n_labels++;
    labels[n] = labels[r];  // assign the label
  }
  
  free(parents);
  return n_labels;
}



static float* tab = NULL;
static int argcmp( const void* i, const void* j ) {
  float diff = tab[*(int*)i] - tab[*(int*)j];
  return diff < 0 ? -1 : diff > 0 ? 1 : 0;
}


/*  res must have size of at least (len(nodes)-1) 
    return the number of selected edges (might be less than allocated)
*/
int _minimum_spanning_tree( coo_matrix* graph, coo_matrix* res ) {
  assert(graph->nr==graph->nc); // matrix is square
  assert(res->nr==res->nc); // matrix is square
  int n,n_nodes = graph->nr;
  assert(res->n_elem >= n_nodes-1 || !"error: not enough space allocated for result");
  
  // Kruskal's Algorithm
  // build initial forest = single node trees
  int* parents = NEWA(int, n_nodes);
  int* ranks = NEWAC(int, n_nodes);
  for(n=0; n<n_nodes; n++)
    parents[n]=n; // default: one set for each node
  
  // sort edge by increasing costs
  int* edge_order = NEWA(int, graph->n_elem);
  for(n=0; n<graph->n_elem; n++)
    edge_order[n] = n; 
  tab = graph->data;
  qsort( edge_order, graph->n_elem, sizeof(int), argcmp );
  
  // now iterate 
  int n_sel_edges=0, cur_edge=0;
  while((n_sel_edges<n_nodes-1) && (cur_edge<graph->n_elem)) {
    
    // pop edge with minimum cost
    int num_edge = edge_order[cur_edge++];
    int n1 = graph->row[num_edge];
    int n2 = graph->col[num_edge];
    float cost = graph->data[num_edge];
    
    if(Union(parents, ranks, n1, n2)) {
      res->row[n_sel_edges] = n1;
      res->col[n_sel_edges] = n2;
      res->data[n_sel_edges] = cost;
      n_sel_edges++;
    }
  }
  
  // free memory
  free(edge_order);
  free(parents);
  free(ranks);
  
  return n_sel_edges;
}




/* Return the element-wise minimum (absent values are considered as INF).
*/
void _union_minimum( csr_matrix* csr1, csr_matrix* csr2, csr_matrix* res) {
  ASSERT_SAME_CSR_SIZE(csr1,csr2);
  const int nr = csr1->nr;
  
  int* indptr = NEWA(int, nr+1);
  vector<int> indices;
  vector<float> data;
  data.reserve(MAX(csr1->indptr[nr],csr2->indptr[nr]));
  indices.reserve(MAX(csr1->indptr[nr],csr2->indptr[nr]));
  
  int r;
  for(r=0; r<nr; r++) {
    indptr[r] = (int)data.size();
    int i = csr1->indptr[r], endi=csr1->indptr[r+1];
    assert(i+1>=endi || csr1->indices[i]<csr1->indices[endi-1]);
    int j = csr2->indptr[r], endj=csr2->indptr[r+1];
    assert(j+1>=endj || csr2->indices[j]<csr2->indices[endj-1]);
    
    while(i<endi && j<endj) {
      if( csr1->indices[i] < csr2->indices[j] ) {
        indices.emplace_back(csr1->indices[i]);
        data.emplace_back(csr1->data[i]);
        i++;
      } else 
      if( csr1->indices[i] > csr2->indices[j] ) {
        indices.emplace_back(csr2->indices[j]);
        data.emplace_back(csr2->data[j]);
        j++;
      } else {
        indices.emplace_back(csr1->indices[i]);
        data.emplace_back(MIN(csr1->data[i],csr2->data[j]));
        i++; j++;
      }
    }
    while(i<endi) {
      indices.emplace_back(csr1->indices[i]);
      data.emplace_back(csr1->data[i]);
      i++;
    }
    while(j<endj) {
      indices.emplace_back(csr2->indices[j]);
      data.emplace_back(csr2->data[j]);
      j++;
    }
  }
  indptr[r] = (int)data.size();
  
  assert(!res->indptr);
  res->nr = nr;
  res->nc = csr1->nc;
  res->indptr = indptr;
  res->indices = indices.data();
  res->data = data.data();
  memset(&data,0,sizeof(data)); // wont release memory
  memset(&indices,0,sizeof(indices));// wont release memory
}


/* Only Retain elements in csr1 that exists in csr2 (at same positions).
  add_missing  ==0: do nothing
               ==1: add elems in csr2 missing in csr1
               ==2: assert if there are elems in csr2 missing in csr1
*/
void _intersect_left( csr_matrix* csr1, csr_matrix* csr2, int add_missing, csr_matrix* res) {
  ASSERT_SAME_CSR_SIZE(csr1,csr2);
  const int nr = csr1->nr;
  
  int* indptr = NEWA(int, nr+1);
  vector<int> indices;
  vector<float> data;
  data.reserve(csr2->indptr[nr]);
  indices.reserve(csr2->indptr[nr]);
  
  int r;
  for(r=0; r<nr; r++) {
    indptr[r] = (int)data.size();
    int i = csr1->indptr[r], endi=csr1->indptr[r+1];
    assert(i+1>=endi || csr1->indices[i]<csr1->indices[endi-1]);
    int j = csr2->indptr[r], endj=csr2->indptr[r+1];
    assert(j+1>=endj || csr2->indices[j]<csr2->indices[endj-1]);
    
    while(i<endi && j<endj) {
      if( csr1->indices[i] < csr2->indices[j] ) {
        i++;
      } else 
      if( csr1->indices[i] > csr2->indices[j] ) {
        switch( add_missing ) {
        case 1:
          indices.emplace_back(csr2->indices[j]);
          data.emplace_back(csr2->data[j]);
          break;
        case 2: 
          assert("error: there are elements csr2 that does not exist in csr1");
        }
        j++;
      } else {
        indices.emplace_back(csr1->indices[i]);
        data.emplace_back(csr1->data[i]);
        i++; j++;
      }
    }
    while(j<endj) {
      switch( add_missing ) {
        case 1:
        indices.emplace_back(csr2->indices[j]);
        data.emplace_back(csr2->data[j]);
          break;
      case 2: 
        assert("error: there are elements csr2 that does not exist in csr1");
      }
      j++;
    }
  }
  indptr[r] = (int)data.size();
  
  assert(!res->indptr);
  res->nr = nr;
  res->nc = csr1->nc;
  res->indptr = indptr;
  res->indices = indices.data();
  res->data = data.data();
  memset(&data,0,sizeof(data)); // wont release memory
  memset(&indices,0,sizeof(indices));// wont release memory
}








































