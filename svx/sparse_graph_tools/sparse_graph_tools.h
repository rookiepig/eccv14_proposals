#pragma once
#include "numpy_image.h"
#include <vector>

/* Find connected component in a graph.
   Return component labels for each graph node.
*/
int find_connected_components(coo_matrix* graph, int_array* res);

/*  res must have size of at least (len(nodes)-1) 
    return the number of selected edges (might be less than allocated)
*/
int _minimum_spanning_tree( coo_matrix* graph, coo_matrix* res );



/* Return the element-wise minimum (absent values are considered as INF).
*/
void _union_minimum( csr_matrix* csr1, csr_matrix* csr2, csr_matrix* res_out);


/* Only Retain elements in csr1 that exists in csr2 (at same positions).
  add_missing  ==0: do nothing
               ==1: add elems in csr2 missing in csr1
               ==2: assert if there are elems in csr2 missing in csr1
*/
void _intersect_left( csr_matrix* csr1, csr_matrix* csr2, int add_missing, csr_matrix* res_out);




























