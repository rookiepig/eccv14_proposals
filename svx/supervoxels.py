
"""Hierarchical segmentation algorithm."""

import argparse
import itertools
import os
import pdb
import sys

from PIL import Image
import matplotlib.pyplot as plt
import numpy as np

from scipy import sparse
from scipy.io import loadmat
import scipy.ndimage

import colors as colutils
import convol
import common
import disttrf
import slic
import sparse_clustering as spclu
import sparse_graph_tools as sptools

from datasets import DATASETS
from extract_features import change_extension


plt.ion()


# Superpixel connection parameters.
compactness = 15.0
min_border_size = 1  # Percentage of the supervoxel perimeter.

# Lab distance parameters
lab_noborder = 2     # Compute lab average without SP border.
lab_pow = 0.5        # 1 means squared, 0.5 means normal
lab_crop = 3.6       # crop distances above this threshviz
lab_w = 0.0          #
lab_wt = 0.95        # temporal connection by diff of mean SP color
lab_ponct_wt = 0.35  # temporal connection by diff of just matched pixel

# color_histogram parameters
ch_L_bin = 20
ch_ab_bin = 14
ch_dist_type = 12  # 0:1-intersection, 1:L1-dist, 2:L2-dist, 12:chi2-kernel
ch_pow = 2.25
ch_w = 0.1
ch_wt = 0.35

# motion boundaries
mb_acc = 25     # boundary accentuation
mb_renorm = 0.18  # mb_h are renormalized by 1/(mb_renorm+mb.max())
mb_w = 0.7

# flow histogram
fh_o_bin = 8
fh_norm_pow = 0.367
fh_norm_ninth = 0.212
fh_dist_type = 12  # 0:1-intersection, 1:L1-dist, 2:L2-dist, 12:chi2-kernel
fh_pow = 1.0
fh_w = 0.9
fh_wt = 0.0

# edges
ed_euc = 0.001  # euclidean overhead cost for distance maps
ed_acc = 20  # accentuate edges to remove weak areas
ed_linkage = 's'
ed_w = 0.65

# other params
ngh_hop = 1
ngh_hop_cost = 1.0
add_null_flow = 0
recompute_color_after_dt = 0

# hierarchical clustering
time_penalty_mode = -11
time_penalty = 3.0

# general
savedges_path = savetree = ''
list_nc = []
clean_clusters = 0
eval_gt = 0
eval_prec_pow = 1
verbose = 0


def load_images(images_path, images_names, verbose=0):

    images = [np.array(Image.open(os.path.join(images_path, im)).convert('RGB')) for im in images_names]
    lab_images = [colutils.rgb_to_lab(im, color_attenuation=1.5) for im in images]

    if verbose:
        print 'Loaded %d images from %s.' % (len(images), images_path)

    # Sanity check that size is consistent across images.
    height, width = images[0].shape[:2]
    for ii, im in enumerate(images):
        assert im.shape[:2] == (height, width), "Image #%d has different size from the first one." % ii

    return images, lab_images


def load_sed_edges(edges_path, image_name, verbose=0):

    path = os.path.join(edges_path, change_extension(image_name, 'mat'))
    if verbose:
        print 'Loading SED edges from ' + path

    sed_edges_mat = loadmat(path)['edges']
    sed_edges = np.ascontiguousarray(sed_edges_mat.astype(np.float32))

    if ed_acc:
        convolved = convol.symmetric_convolution(sed_edges, (ed_acc, -2, -4, -1))
        sed_edges = np.maximum(0, convolved) / 3

    sed_edges *= 1 / (1e-16 + sed_edges.max())
    return sed_edges


def load_flow(flow_paths, image_name, shape, verbose=0):
    return common.load_flow(
        flow_paths,
        change_extension(image_name, 'flo'),
        shape=shape,
        verbose=verbose)


def compute_edge_weights(sed_edges, flows, viz, verbose=0):

    def compute_motion_boundaries(flow):
        motion_boundaries = np.sqrt(common.sqgrad(flow, -1))
        if mb_acc:
            convolved = convol.symmetric_convolution(motion_boundaries, (mb_acc, -2, -4, -1))
            motion_boundaries = np.maximum(0, convolved) / 3
        motion_boundaries *= 1 / (motion_boundaries.max() + mb_renorm + 1e-16)
        return motion_boundaries

    edge_weigths = []

    for ii, (sed_edge, flow) in enumerate(itertools.izip(sed_edges, flows)):

        motion_boundaries = compute_motion_boundaries(flow)
        edges = ed_euc + ed_w * sed_edge + mb_w * motion_boundaries

        edge_weigths.append(edges)

        if 'flow' in viz and ii < len(flows) - 1:
            viz['flow'](ii, flow)

        if 'mb' in viz:
            viz['mb'](ii, motion_boundaries)

        if 'edges' in viz:
            viz['edges'](ii, sed_edge, edges)

    return edge_weigths


def prepare_viz(viz, images):

    from viz_tree import (
        recolored,
        show_flow_mouse,
        show_matches_mouse,
        show_pairs,
        shuffled,
    )

    def viz_cmp_ch(ii, ch_ngh, nodes, label):
        show_pairs(images[ii], ch_ngh, nodes, label, -20)

    def viz_cmp_ed_mb(ii, ed_mb_ngh, nodes, dtlabel):
        show_pairs(images[ii], ed_mb_ngh, nodes, dtlabel, -20)

    def viz_cmp_hop(ii, ngh, nodes):
        show_pairs(images[ii], ngh, nodes, None, 20)

    def viz_cmp_fh(ii, fh_ngh, nodes, label):
        show_pairs(images[ii], fh_ngh, nodes, label, -20)

    def viz_cmp_lab(ii, lab_ngh, nodes, label):
        show_pairs(images[ii], lab_ngh, nodes, label, -20)

    def viz_cmp_ngh(ii, ngh, nodes, label):
        show_pairs(images[ii], ngh, nodes, label, -20)

    def viz_dmaps(ii, edges, dmaps, nodes):
        plt.clf()
        plt.subplot(311)
        plt.imshow(images[ii])
        plt.subplot(312)
        plt.imshow(edges, vmin=0)
        plt.subplot(313)
        plt.imshow(dmaps[ii], vmin=0)
        plt.plot(nodes[:, 0], nodes[:, 1], '+', c=(0, 1, 0), ms=5, scalex=0, scaley=0)
        pdb.set_trace()

    def viz_edges(ii, sed_edges, edges):
        plt.subplot(311)
        plt.imshow(images[ii])
        plt.subplot(312)
        plt.imshow(sed_edges, vmin=0)
        plt.subplot(313)
        plt.imshow(edges, vmin=0)
        pdb.set_trace()

    def viz_intermatches(ii, corres, costs):
        plt.clf()
        show_matches_mouse(images[ii], images[ii + 1], corres)
        ax1 = plt.subplot(211)
        ax2 = plt.subplot(212)
        print 'Showing matches from worst to best'
        for j in costs.argsort()[::-5]:
            ax1.lines = []
            ax2.lines = []
            x0, y0, x1, y1 = corres[j, :4].astype(np.int32)
            print costs[j], (x0, y0, x1, y1), corres[j, 4]
            plt.subplot(211)
            plt.plot(corres[j, 0], corres[j, 1], '+', ms=10, c=(0, 1, 0), scalex=0, scaley=0)
            plt.subplot(212)
            plt.plot(corres[j, 2], corres[j, 3], '+', ms=10, c=(0, 1, 0), scalex=0, scaley=0)
            if raw_input() != '':
                break

    def viz_flow(ii, flow):
        show_flow_mouse(images[ii], images[ii + 1], flow)

    def viz_motion_boundaries(ii, motion_boundaries):
        plt.subplot(211)
        plt.imshow(images[ii])
        plt.subplot(212)
        plt.imshow(motion_boundaries, vmin=0, vmax=1)
        pdb.set_trace()

    def viz_slic(ii, labels, nodes):
        plt.clf()
        plt.subplot(311)
        plt.imshow(images[ii], interpolation='nearest')
        plt.plot(nodes[:, 0], nodes[:, 1], '+', ms=10, c='k', scalex=0, scaley=0)
        plt.subplot(312)
        plt.imshow(recolored(labels, nodes), interpolation='nearest')
        plt.plot(nodes[:, 0], nodes[:, 1], '+', ms=10, c='k', scalex=0, scaley=0)
        plt.subplot(313)
        plt.imshow(shuffled(labels), interpolation='nearest')
        plt.plot(nodes[:, 0], nodes[:, 1], '+', ms=10, c='k', scalex=0, scaley=0)
        pdb.set_trace()

    VIZ_TO_FUNC = {
        'cmp_ch': viz_cmp_ch,
        'cmp_ed_mb': viz_cmp_ed_mb,
        'cmp_hop': viz_cmp_hop,
        'cmp_fh': viz_cmp_fh,
        'cmp_lab': viz_cmp_lab,
        'cmp_ngh': viz_cmp_ngh,
        'dmaps': viz_dmaps,
        'edges': viz_edges,
        'intermatches': viz_intermatches,
        'flow': viz_flow,
        'mb': viz_motion_boundaries,
        'slic': viz_slic,
    }

    return {vv: VIZ_TO_FUNC[vv] for vv in viz if vv in VIZ_TO_FUNC}


def compute_matches(lab_images, flows):

    matches = []

    blur_images = 0.5
    image = convol.gaussian_blur(lab_images[0], blur_images)

    match_sub = 4  # Subsampling.
    height, width = lab_images[0].shape[1:]
    grid = np.mgrid[match_sub / 2: height: match_sub, match_sub / 2: width: match_sub][::-1].astype(np.float32)
    xyfield = np.rollaxis(grid, 0, 3)

    for ii, flow in enumerate(flows[:-1]):

        corres = np.hstack((
            xyfield.reshape(-1, 2),
            (flow[match_sub / 2::match_sub, match_sub / 2::match_sub] + xyfield).reshape(-1, 2),
            np.zeros((xyfield.size / 2, 1), dtype=np.float32)))

        if add_null_flow:
            corres2 = np.hstack((
                xyfield.reshape(-1, 2),
                xyfield.reshape(-1, 2),
                np.zeros((xyfield.size / 2, 1), dtype=np.float32)))
            if add_null_flow < 0:
                corres = corres2
            else:
                corres = np.vstack((corres, corres2))

        # Remove bad correspondences.
        kept = corres[:, 2:4].min(1) >= 0
        kept &= corres[:, 2] < width
        kept &= corres[:, 3] < height
        corres = corres[kept]

        # Weight correspondences based on lab distance.
        next_image = convol.gaussian_blur(lab_images[ii + 1], blur_images)
        x0, y0, x1, y1 = corres.T[:4].astype(np.int32)
        corres[:, 4] = np.sum((image[:, y0, x0] - next_image[:, y1, x1]) ** 2, axis=0)
        corres[:, 4] **= lab_pow
        corres[:, 4] /= 100.0 ** lab_pow

        image = next_image
        matches.append(corres)

    return matches


def compute_slic(nb_slic, lab_images, edge_weights, viz, verbose):

    n_node = 0

    list_nodes = []
    list_labels = []

    for ii, (lab, edges) in enumerate(zip(lab_images, edge_weights)):

        if verbose:
            print 'Computing SLIC for frame %d.' % ii

        nl, labels = slic.compute_SLIC(
            lab,
            sp_nb=nb_slic,
            compactness=compactness,
            contiguous=True,
            perturbseeds=True,
            n_iter=10,
            extend_window=1)

        list_labels.append(labels)

        # Recompute SLIC colors based on new dist-trf labels.
        nodes = slic.recompute_seeds(lab, nl, labels, noboundaries=lab_noborder)

        # Refine SLIC position far from image edges.
        edges = convol.gaussian_blur(edges, 4)
        boundaries = convol.gaussian_blur(slic.superpixel_boundaries(labels).astype(np.float32), 2)
        avoid_map = edges + boundaries
        pos = scipy.ndimage.measurements.minimum_position(avoid_map, labels=labels, index=np.arange(nl))
        nodes[:, 0:2] = np.array([(x, y) for y, x in pos], dtype=np.float32)
        list_nodes.append(nodes)
        n_node += len(nodes)

        if 'slic' in viz:
            viz['slic'](ii, labels, nodes)

    return list_nodes, list_labels


def compute_distance_transform(list_nodes, edge_weights, list_matches, viz, min_change=1, verbose=0):

    dt_params = disttrf.dt_params(min_change=0.01, max_iter=40)

    n_images = len(edge_weights)
    height, width = edge_weights[0].shape

    dmaps = np.ones((n_images, height, width), dtype=np.float32)
    dmaps *= np.inf
    labels = np.empty((n_images, height, width), dtype=np.int32)

    for ii, (nodes, edges) in enumerate(zip(list_nodes, edge_weights)):

        if verbose:
            print 'Computing distance transform for frame %d.' % ii

        x, y = nodes[:, :2].astype(np.int32).T
        dmaps[ii][y, x] = 0
        labels[ii][y, x] = np.arange(len(nodes), dtype=np.int32)

        disttrf._weighted_distance_transform(edges, dt_params, dmaps[ii], labels[ii])

        if 'dmaps' in viz:
            viz['dmaps'](ii, edges, dmaps, nodes)

    return dmaps, labels


def prune_hopped_edges(ngh, color_ngh2, ngh_hop, coef_secure=1, offset=0.01):
    # Propagate 'normal' (touching) edges.
    ngh_prop = sptools.shortest_dist_nhops(ngh, ngh_hop)
    common.check_symmetric(ngh_prop)
    # Retain elements in ngh_prop that exists in color_ngh2.
    ngh_prop = sptools.intersect_left(ngh_prop, color_ngh2, 2)  # Assert if missing

    assert all(ngh_prop.indptr == color_ngh2.indptr)
    assert all(ngh_prop.indices == color_ngh2.indices)
    diff = ngh_prop.data - coef_secure * color_ngh2.data
    color_ngh2.data[diff < offset] = 0
    color_ngh2.eliminate_zeros()
    common.check_symmetric(color_ngh2)
    return color_ngh2


def compute_lab_distances(ngh, nodes):
    disttrf.compare_colors(ngh, nodes, bool(1), bool(1))
    ngh.data **= lab_pow
    ngh.data /= 100 ** lab_pow
    ngh.data[ngh.data > lab_crop] = lab_crop
    return ngh


def prepare_color_hist(labimg, labels, n_labels):
    labimg = np.rollaxis(labimg, 0, 3)
    return colutils.compute_color_hist(labimg, labels, n_labels=n_labels, L_bin=ch_L_bin, ab_bin=ch_ab_bin)


def compute_color_hist_distances(ngh, col_hist):
    colutils.compare_color_hist(ngh, col_hist, ch_dist_type)
    ngh.data **= ch_pow
    return ngh


def prepare_flow_hist(flow, labels, n_labels):
    return colutils.compute_flow_hist(flow, labels, n_labels=n_labels, n_bin=fh_o_bin, flow_pow=fh_norm_pow, ninth=fh_norm_ninth)


def compute_flow_hist_distances(fh_ngh, flow_hist):
    colutils.compare_flow_hist(fh_ngh, flow_hist, fh_dist_type)
    fh_ngh.data **= fh_pow
    return fh_ngh


def compute_graph(lab_images, list_flows, list_nodes, list_labels, list_matches, dmaps, dtlabels, viz, verbose=0):
    """The graph weights are based on the following distance between two neighboring superpixels:

      d(mean lab) + d(color histogram) + d(motion boundaries) + d(flow histogram) + d(edges).

    Accros time: same, without motion boundaries and edges.

    2-hop neighbours: only lab + color histogram + flow histogram + constant penalty
    Note: removed if useless (can be reached in 2x 1-hop with inferior cost).

    """

    if verbose:
        print 'Merging connectivity graphs.'

    nl = 0
    graph = []
    all_color_hist = []
    all_flow_hist = []

    n_node = sum(map(len, list_nodes))

    for ii, (nodes, dmap, dtlabel) in enumerate(zip(list_nodes, dmaps, dtlabels)):

        if verbose:
            print 'Computing and merging color distances between superpixels in frame %d.' % ii

        # Edge + motion boundary.
        ed_mb_ngh = disttrf.ngh_labels_to_spmat(len(nodes), dtlabel, dmap, linkage=ed_linkage, min_border_size=min_border_size)
        nr_neighbors = ed_mb_ngh.indptr[1:] - ed_mb_ngh.indptr[:-1]
        assert all(nr_neighbors > 0), "Error: disconnected elements."

        if 'cmp_ed_mb' in viz:
            viz['cmp_ed_mb'](ii, ed_mb_ngh, nodes, dtlabel)

        # Color.
        if recompute_color_after_dt:
            label = list_labels[ii] = dtlabel  # Replace labels.
            nodes[:, 2:5] = slic.recompute_seeds(lab_images[ii], len(nodes), dtlabel, noboundaries=lab_noborder)[:, 2:5]
        else:
            label = list_labels[ii]
        lab_ngh = compute_lab_distances(ed_mb_ngh.copy(), nodes)

        if 'cmp_lab' in viz:
            viz['cmp_lab'](ii, lab_ngh, nodes, label)

        # Color histograms.
        all_color_hist.append(prepare_color_hist(lab_images[ii], label, len(nodes)))
        ch_ngh = compute_color_hist_distances(ed_mb_ngh.copy(), all_color_hist[-1])

        if 'cmp_ch' in viz:
            viz['cmp_ch'](ii, ch_ngh, nodes, label)

        # Flow histograms.
        all_flow_hist.append(prepare_flow_hist(list_flows[ii], label, len(nodes)))
        fh_ngh = compute_flow_hist_distances(ed_mb_ngh.copy(), all_flow_hist[-1])

        if 'cmp_fh' in viz:
            viz['cmp_fh'](ii, fh_ngh, nodes, label)

        # Merge first neighbor (minimum intersection) with colors_ngh.
        ngh = ed_mb_ngh.copy()
        ngh.data += lab_w * lab_ngh.data
        ngh.data += ch_w * ch_ngh.data
        ngh.data += fh_w * fh_ngh.data
        common.check_symmetric(ngh)

        # Incorporate hopped neighbours.
        if ngh_hop > 1:
            ngh2 = sptools.find_neighbours(ngh, rad=ngh_hop, radmin=2)
            ngh2.data[:] = ngh_hop_cost
            assert ngh.multiply(ngh2).nnz == 0

            ngh2.data += lab_w * compute_lab_distances(ngh2.copy(), nodes).data
            ngh2.data += ch_w * compute_color_hist_distances(ngh2.copy(), all_color_hist[-1]).data
            ngh2.data += fh_w * compute_flow_hist_distances(ngh2.copy(), all_flow_hist[-1]).data

            # Prune useless edges.
            ngh2 = prune_hopped_edges(ngh, ngh2, ngh_hop, coef_secure=1)

            if 'cmp_hop' in viz:
                viz['cmp_hop'](ii, ngh2, nodes)

            ngh = ngh + ngh2  # They can't overlap.
            common.check_symmetric(ngh)

        if 'cmp_ngh' in viz:
            viz['cmp_ngh'](ii, ngh, nodes, label)

        ngh = common.enlarge(ngh, nl, n_node).tocoo()
        graph.append((ngh.row, ngh.col, ngh.data))
        nl += len(nodes)

    # Update multi-frame infos.
    nl = 0
    node_frames = []
    for ii, nodes in enumerate(list_nodes):
        node_frames += [ii] * len(nodes)
        list_labels[ii] += nl
        nl += len(nodes)

    node_frames = np.array(node_frames, dtype=np.int32)
    all_nodes = np.vstack(list_nodes)
    labels = np.vstack([l[None, :,:] for l in list_labels])

    all_flow_hist = np.vstack(all_flow_hist)
    all_color_hist = common.sparse_vstack(all_color_hist)

    # Adding inter-frame links.
    for ii, corres in enumerate(list_matches):
        x0, y0, x1, y1 = corres.T[:4].astype(np.int32)
        label0 = labels[ii, y0, x0]
        label1 = labels[ii + 1, y1, x1]

        # Select best match per superpixels.
        costs = corres[:, 4]
        kept0 = scipy.ndimage.measurements.minimum_position(costs, labels=label0, index=np.unique(label0))
        kept0 = [int(l[0]) for l in kept0]
        kept1 = scipy.ndimage.measurements.minimum_position(costs, labels=label1, index=np.unique(label1))
        kept1 = [int(l[0]) for l in kept1]
        kept = list(set(kept0) & set(kept1))  # Intersection.
        corres = corres[kept]

        label0 = label0[kept]
        label1 = label1[kept]
        costs = costs[kept]
        ngh = sparse.coo_matrix((costs, (label0, label1)), shape=(n_node, n_node), dtype=np.float32).tocsr()

        # recompute costs
        ngh.data *= lab_ponct_wt
        ngh.data += compute_lab_distances(ngh.copy(), all_nodes).data * lab_wt
        ngh.data += compute_color_hist_distances(ngh.copy(), all_color_hist).data * ch_wt
        ngh.data += compute_flow_hist_distances(ngh.copy(), all_flow_hist).data * fh_wt

        if 'intermatches' in viz:
            viz['intermatches'](ii, corres, costs)

        ngh = ngh.tocoo()
        graph.append((ngh.row, ngh.col, ngh.data))
        graph.append((ngh.col, ngh.row, ngh.data))  # And symmetric.

    graph = zip(*graph)
    graph = sparse.coo_matrix((np.hstack(graph[2]), (np.hstack(graph[0]), np.hstack(graph[1]))), shape=(n_node, n_node), dtype=np.float32).tocsr()
    common.check_symmetric(graph)

    return graph, labels, node_frames, all_nodes


def compute_hierarchical_clustering(graph, node_frames, verbose):

    if verbose:
        print 'Performing hierarchical clustering.'

    global time_penalty_mode

    if time_penalty_mode == -11:   # Automatic mode.
        tree = spclu.temporal_tree_cluster(graph, node_frames, penalty_coef=0, mode=1, linkage='a', verbose=1, boost=1)
        time_penalty_mode = 11
        time_penalty = float(tree.view(np.float32)[-1, 2])

        if verbose > 1:
            print 'Time penalty:', time_penalty

    return spclu.temporal_tree_cluster(graph, node_frames, penalty_coef=time_penalty, mode=time_penalty_mode, linkage='a', verbose=1, boost=1)


def main():

    VALID_VIZ = (
        'cmp_ch',
        'cmp_ed_mb',
        'cmp_hop',
        'cmp_fh',
        'cmp_lab',
        'cmp_ngh',
        'dmaps',
        'edges',
        'intermatches',
        'flow',
        'mb',
        'slic',
        'tree',
    )

    parser = argparse.ArgumentParser(description=__doc__)

    parser.add_argument('--video', help="video to work on.")
    parser.add_argument('-d', '--dataset', choices=DATASETS.keys(), default='ucf_sports', help="the name of the data set.")
    parser.add_argument('--start', type=int, help="index of first frame.")
    parser.add_argument('--end', type=int, help="index of last frame.")
    parser.add_argument('-n', '--nr_clusters', nargs='+', type=int, help="number of desired clusters.")
    parser.add_argument('--nr_slic', type=int, default=1000, help="number of SLICs for each frame.")
    parser.add_argument('--viz', nargs='+', default=(), choices=VALID_VIZ, help="what to visualize.")
    parser.add_argument('--save', nargs='+', default=('images'), choices=('images', 'tree'), help="what to save.")
    parser.add_argument('-v', '--verbose', action='count', help="verbosity level.")

    args = parser.parse_args()

    dataset = DATASETS[args.dataset]

    images_path = dataset.get_images_path(args.video)
    edges_path = dataset.get_edges_path(args.video)

    flow_paths = {
        k: dataset.get_flow_path(args.video, k)
        for k in ('forward', 'backward')}

    images_names = [
        im for im in sorted(os.listdir(images_path))
        if im.endswith('.jpg') or im.endswith('.png') or im.endswith('.ppm')]
    images_names = images_names[args.start: args.end]

    images, lab_images = load_images(images_path, images_names, args.verbose)
    visualizations = prepare_viz(args.viz, images)

    flow_shape = len(images_names) == 1 and images[0].shape

    sed_edges = [load_sed_edges(edges_path, im, args.verbose) for im in images_names]
    flows = [load_flow(flow_paths, im, flow_shape, args.verbose) for im in images_names]
    edge_weights = compute_edge_weights(sed_edges, flows, visualizations, args.verbose)

    list_matches = compute_matches(lab_images, flows)
    list_nodes, list_labels = compute_slic(args.nr_slic, lab_images, edge_weights, visualizations, verbose)

    dmaps, dtlabels = compute_distance_transform(list_nodes, edge_weights, list_matches, visualizations, min_change=0.1, verbose=args.verbose)

    # Build proximity graph: for each node, compare to the neighboring nodes.
    graph, labels, node_frames, all_nodes = compute_graph(lab_images, flows, list_nodes, list_labels, list_matches, dmaps, dtlabels, visualizations, verbose=args.verbose)
    tree = compute_hierarchical_clustering(graph, node_frames, verbose=args.verbose)

    all_data = {
        'all_nodes': all_nodes,
        'graph': graph,
        'images_names': images_names,
        'labels': labels,
        'node_frames': node_frames,
        'time_penalty': time_penalty,
        'time_penalty_mode': time_penalty_mode,
        'tree': tree,
    }

    if 'tree' in args.viz:

        class Struct:
            def __init__(self, **kwargs):
                for k, v in kwargs.items():
                    setattr(self, k, v)

        from viz_tree import (
            display,
            study_potential_merge,
            track_merge,
            try_merge,
            viz_tree,
        )

        n_node = sum(map(len, list_nodes))
        data = Struct(n_node=n_node, **all_data)

        def display_func(n_clusters, final_labels, true_colors, show_cut, study_func, imrange):
            return display(
                data, n_clusters, final_labels, true_colors, show_cut,
                study_func, imrange, (), (), args.verbose)

        viz_tree(n_node, tree, display_func, verbose=args.verbose)

    if 'tree' in args.save:
        tree_path = dataset.get_tree_path(args.video)
        common.save_hierarchy(tree_path, tree, labels)

    if 'images' in args.save:
        segmentation_dir = dataset.get_segmentation_directory(args.video)
        common.save_result_ppm(segmentation_dir, all_data, clean_clusters=clean_clusters, list_nc=list_nc, verbose=args.verbose)


if __name__ == '__main__':
    main()
